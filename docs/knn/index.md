---
title: Une application de KNN
author: Marius Monnier
---

Le but de l'activité est d'utiliser l'algorithme **KNN** pour prédire votre comportement.
Comme nous n'allons pas mesurer vos données de trajet ou de comportement en classe, nous devons trouver autre chose.

Ici nous allons donc mesurer votre *reconnaissance des couleur* et votre *classification*

# Le TP

Commencez par ouvrir Thonny et créer un nouveau fichier vide nommé `couleurs.py`.
Dans ce fichier copiez le code suivant et exécutez le.

```python linenums="1"
import turtle as t
import matplotlib.pyplot as plt
from random import randint
import tkinter as tk

# On commence par définir les variables concernant les couleurs à classer.

def rcol():
    ''' renvoie une couleur aléatoire dans le rouge et le vert.
    '''
    return randint(0,255),randint(0,255),randint(0,255)
# La liste des couleurs et leur classification
NB_COULEURS = 5
INDICE_COULEUR_ACTUELLE = 0
couleurs = [{'couleur':rcol(),'classe':''} for _ in range(NB_COULEURS)]

# On définit ensuite toutes les fonctions nécessaires
# pour les boutons.

def enregistre_sombre():
    '''
    Définit la classe à sombre pour la couleur actuelle, repérée par la variable
    globale INDICE_COULEUR_ACTUELLE.
    '''
    global INDICE_COULEUR_ACTUELLE
    global couleurs
    global fini
    
    # On classifie la couleur comme sombre.
    couleurs[INDICE_COULEUR_ACTUELLE]['classe'] = 'sombre'
    
    # On passe à la couleur suivante.
    INDICE_COULEUR_ACTUELLE += 1
    
    # Si c'était la dernière couleur, le programme est fini
    if INDICE_COULEUR_ACTUELLE >= len(couleurs):
        fini = True
        return
    
    # Sinon on redessine l'écran
    dessine_ecran()
    
def enregistre_clair():
    '''
    Définit la classe à clair pour la couleur actuelle, repérée par la variable
    globale INDICE_COULEUR_ACTUELLE.
    '''
    global INDICE_COULEUR_ACTUELLE
    global couleurs
    global fini
    

    
    couleurs[INDICE_COULEUR_ACTUELLE]['classe'] = 'clair'
    INDICE_COULEUR_ACTUELLE += 1
    
    if INDICE_COULEUR_ACTUELLE >= len(couleurs):
        fini = True
        return
    
    dessine_ecran()

def dessine_ecran():
    '''
    Dessiner un rectangle de la couleur montrée par INDICE_COULEUR_ACTUELLE.
    '''
    global couleurs
    global INDICE_COULEUR_ACTUELLE
    couleur = couleurs[INDICE_COULEUR_ACTUELLE]['couleur']
    TAILLE_COULEUR=50
    tortue.fillcolor(couleur)
    tortue.begin_fill()
    
    tortue.goto(0,0)
    tortue.goto(0,TAILLE_COULEUR)
    tortue.goto(TAILLE_COULEUR,TAILLE_COULEUR)
    tortue.goto(TAILLE_COULEUR,0)
    tortue.goto(0,0)
    
    tortue.end_fill()

def stop():
    global fini
    fini = True

# La tortue pour dessiner la couleur
tortue = t.Turtle()
tortue.hideturtle()
t.colormode(255)
t.speed(10)

# L'écran pour les boutons
root = tk.Tk()
root.title("Turtle et Tkinter")

# Création des boutons
button_frame = tk.Frame(root)
button_frame.pack()

sombre = tk.Button(button_frame, text="Sombre", command=enregistre_sombre)
sombre.pack(side=tk.LEFT)

clair = tk.Button(button_frame, text="Clair", command=enregistre_clair)
clair.pack(side=tk.LEFT)

fin = tk.Button(button_frame, text="Fin", command=stop)
fin.pack(side=tk.LEFT)


# Programme principal, on dessine chaque couleur.

dessine_ecran()
fini = False
while not fini:
    root.update()

root.destroy()
t.bye()
```

Vous pouvez utiliser une IA pour vous expliquer le code ou bien demander à votre professeur.

## Premières modifications

* Cherchez comment changer la couleur de fond des boutons 'Sombre' et 'Clair' pour qu'on ne les confondent pas;
* affichez à la fin les différentes classes et couleurs décidées.
* Complétez la fonction `classe_majoritaire` qui permet de déterminer si les couleurs sont majoritairement sombres ou claires.

```python title="La fonction classe_majoritaire"
def classe_majoritaire(couleurs):
    occurences = ... # Le nombre de fois où chaque classe apparait, représenté par un dictionnaire
    for c in couleurs
        if ...: #Si la classe de c est présente dans occurences on augmente sa valeur de 1
            occurences[c] = ...
        else: #On doit l'initialiser à 1
            occurences[c] = ...
    return max(occurences,key=occurences.get)
```

* Utiliser cette fonction pour afficher la classe majoritaire d'un ensemble de couleurs.

## Modifications avancées (KNN)

Jusqu'ici vous êtes obligés de recliquer à chaque fois que vous voulez tester, il serait quand même plus pratique de sauvegarder au lieu de les recréer à chaque fois.

Pour cela nous allons changer la logique du code.

Tout d'abord, après avoir affiché les couleurs, nous allons les enregistrer à l'aide du module CSV.

Pour cela voici la fonction qui permet d'écrire un dictionnaire `d` dans un fichier de nom `f`:

```python title="Ecriture dans un fichier CSV"
import csv
def ecrire_csv(d,f):
    with open(f, 'w', newline='') as csvfile:
        fieldnames = d.keys()
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        writer.writerow({k: str(v) for k, v in d.items()})
```

Et la fonction à compléter pour créer le dictionnaire à partir du fichier CSV

```python title="Lire un fichier CSV"
import csv
def reconstruire_dictionnaire_depuis_csv(f):
    d = ... # Créer un dictionnaire vide que l'on remplit petit à petit.

    with open(f, 'r', newline='') as csvfile:
        lecteur = csv.DictReader(csvfile)
        for ligne in lecteur:
            for cle, valeur in ligne.items():
                # Convertir les valeurs en tuple si elles sont représentées comme des chaînes de caractères
                if ...: #Si il y a des parenthéses dans la valeur.
                    valeur = tuple(map(int, valeur.strip('()').split(',')))
                d[...] = ... # on associe la clé et la valeur dans le dictionnaire.

    return ... # Renvoyer le dictionnaire créé
```

* Ajouter la fonction pour écrire le fichier CSV et l'appeler pour enregistrer un ensemble de 10 couleurs et leur classification.
* Dans un nouveau fichier `knn.py`, copiez le code pour reconstruire le dictionnaire et complétez le.
* Ensuite copiez le code suivant qui utilise la fonction pour construire le dictionnaire et l'affiche sous forme de graphique.

```python linenums="1"
import csv
def reconstruire_dictionnaire_depuis_csv(f):
    ...

couleurs = reconstruire_dictionnaire_depuis_csv(...)
    
def affiche(couleurs):
    red = [c['couleur'][0] for c in couleurs]
    green = [c['couleur'][1] for c in couleurs]
    
    sombres = [c['couleur'] for c in couleurs if c['classe'] == 'sombre']
    clairs = [c['couleur'] for c in couleurs if c['classe'] == 'clair']
    
    plt.scatter([s[0] for s in sombres],[s[1] for s in sombres],marker='o')
    plt.scatter([c[0] for c in clairs],[c[1] for c in clairs],marker='+')
    plt.show()

affiche(couleurs)
```

On peut maintenant faire l'algorithme KNN pour déterminer si une couleur donnée est sombre ou claire.

* Copiez et complétez le code suivant.

```python

def distance_couleurs(c1,c2):
    '''La distance euclidienne entre deux couleurs vues comme deux points de l'espace.'''
    return ...

def distance(element,liste):
    '''On crée une liste contenant les dictionnaires de liste avec un attribut en plus
        qui est la distance entre element et les éléments de la liste
    '''
    ld = []
    for e in liste:
        ld.append({'classe':...,'couleur':...,'distance':...})
    return ld

def knn(element,liste):
    '''Prédiction de la classe de la couleur element
    '''
    liste_distance = ...(element,liste)
    liste_triee = ...(liste_distance,key=lambda e:e['distance'])
    kliste = liste_triee[:k]
    return ...(kliste)
```

* Essayez votre algorithme KNN sur quelques couleurs que vous choisirez, vérifiez à l'aide de turtle ou d'un site web que vous auriez fait la même classification.

## Mise à jour de l'application de classification des couleurs

Vous pouvez désormais modifier l'application pour afficher lors du classement une indication donnée par l'ordinateur.

Celle-ci consistera a exécuter KNN pour décider la classe de la nouvelle couleur aléatoire et l'afficher si elle est différente de celle que vous choisissez dans l'application.

Il faut revoir le code en profondeur dans cette partie, n'hésitez pas à utiliser le spoiler suivant pour vous aider.

??? tip "Indice 1"

??? tip "Indice 2"