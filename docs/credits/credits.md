---
author: Marius Monnier
title: Crédits
---

Le site est hébergé par la forge de [l'*Association des Enseignants d'Informatique de France*](https://aeif.fr/index.php/accueil/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

😀 Un grand merci à  [Mireille COILHAC]() [Vincent-Xavier Jumel](https://forge.aeif.fr/vincentxavier),[Vincent Bouillot](https://gitlab.com/bouillotvincent)et [Charles Poulmaire](https://forge.aeif.fr/cpoulmaire) qui ont réalisé le modèle de ce site.

L'adresse originale du rendu de ce site modèle avant clonage est : 

[Modèle de projet avec Python](https://modeles-projets.forge.aeif.fr/mkdocs-pyodide-review/)

