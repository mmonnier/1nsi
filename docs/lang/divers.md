# Diversité des langages de programmation

En informatique, de multiples langages sont utilisés pour créer des logiciels et traiter des données.

Au long de l'année, vous avez surtout manipulé **Python** et un peu **HTML**.
Ce ne sont que deux langages parmi des *centaines* de langages, plus ou moins spécialisés et plus ou moins *facile* en terme d'apprentissage.

On se concentre ici sur les langages dits **impératifs** et **déclaratifs**, car ce sont ceux qui permettent de **programmer des logiciels**.

??? note En vérité
	Les catégories ne sont pas aussi **immuables** que cela.
	On peut tout à fait utiliser un langage descriptif, mais cela sort du cadre de ce cours.
	Aussi, lors de la conception d'un logiciel, on utilise **simultanément** plusieurs langages de programmations de **paradigmes** différents (fonctionnels, descriptifs, logiques, concurrents, objets ...)

## Séance 1: Diversité et Unité de langages

### Objectifs du Bulletin Officiel

Vous devez connaitre les notions suivantes à la fin de ce cours.

| Notion                     | Commentaire                                                                          |
| -------                    | ----------                                                                           |
| Constructions élémentaires | Séquence, Affectation,Conditionnelles,boucles (bornées ou non), appels de fonctions. |
| Diversité des langages     | Repérer dans un nouveau langage les traits particuliers.                             |
| Unité des langages         | Repérer les similitudes et constructions communes.                                   |

### Questionnaire de Pré-requis

[Cliquez ici pour l'avoir en version PDF](https://cahier-nsi.fr/#!/diagnostic/seq8)

Normalement chacun•e d'entre vous sait répondre à ces questions, si ce n'est pas le cas, vous êtes invité•es à revoir les chapitres sur la programmation python.

### Activité Histoire des Sciences du manuel Bordas Collection 3.0 (Thème F)

*Document distribué en cours*

Dans cette activité vous découvrez la chronologie et les diverses évolutions qui ont menés à la **diversité** des langages que nous utilisons aujourd'hui dans le développement informatique.

!!! info À savoir
	Un **langage bas niveau** est un langage de programmation *proche de la machine*, assez lointain du **langage naturel**.
	Le langage de plus bas niveau qui soit est celui de votre processeur appelé **assembleur**, il se présente sous la forme d'opérations très courtes:

	```x86asm
	MOV R0,$3
	ADD R0,R0,$1
	MOV R1,$0x98
	STORE R1,R0
	```

	Un **langage haut niveau** au contraire est plus proche du langage naturel et donc de *l'humain*.
	Ces langages permettent des *constructions simplifiées* pour accomplir des *taches complexes* et facilitent donc **l'utilisation**.
	Cependant ils ne peuvent être **directement exécutés** par la machine.


La plupart de ces langages sont **équivalents**, qu'ils soient de *haut* ou *bas* niveau, mais ils ont chacun un **domaine** où ils sont plus utilisés.

[Exercices d'application]()

### Activité "Mêmes algorithmes, différentes syntaxes", Hachette NSI 1ère.

Téléchargez l'archive suivante [archive](https://lycee.hachette-education.com/ressources/0002017158370/snsi_1-t01c14-246-activite.zip) et extrayez là dans un répertoire nommé "Activité langages".

L'activité est la suivante: [Page 246-247](https://educadhoc.fr/reader/textbook/9782017158370/fxl/Page_246?feature=freemium)

!!! info À savoir
	Chaque langage a des particularités de **syntaxe** et d'exécution.

	* Typage des variables et paramètres ;
	* Délimitation **explicite** des blocs de programme ;
	* Définition de variable explicite ou *à la volée*;
	* Délimitation des instructions;
	* Écriture de blocs de commentaires pour **expliciter** le programme à l'utilisateur.



### Activité "Constructions élémentaires" (Bordas) et "Comparer deux langages de programmation" (Bordas)

!!! info À savoir

	Malgré leurs différences, les langages proposent tous un ensemble de fonctionnalités semblables.
	
	* L'affectation de variable (`=` et non typée en python)
	* La possibilité de **répéter une suite d'instructions** (de façon bornée ou non en python avec `for` et `while`).
	* L'exécution conditionnelle d'une suite d'instruction (avec `if,elif,else` en python)
	* La séquence d'instructions (délimitée par un **retour à la ligne** en python)
	* L'appel de fonction (sous la forme `fonction(param1,param2,...)` en python.

	Ce sont les **constructions élémentaires** des langages de programmation.
	On les retrouve **sous des formes différentes** quelque soit le langage utilisé, mais les principes **sont toujours les mêmes**.


??? info "Élémentaires vraiment ?"
	Pour qu'un langage de programmation puisse exprimer **tout les programmes** (on dit *Turing-complet*) il ne nécessite pas toutes ces instructions.
	De la même manière qu'une boucle **bornée** peut s'écrire avec une boucle **non bornée**, on peut écrire certaines constructions avec d'autres plus *basiques*.
	La boucle peut par exemple s'écrire avec une instruction dite de **saut conditionnel** qui indique **où aller dans le programme**.
	On retrouve ces constructions **plus élémentaires que élémentaire** dans les langages de très bas niveau comme l'assembleur.

### Observation "Rosetta Code"

De la même manière qu'il a fallu la connaissance de plusieurs langages pour déchiffrer les hiéroglyphes, il est utile de connaitre déjà un langage de programmation pour déchiffrer les autres.

Le site [Rosetta Code](https://rosettacode.org/wiki/Rock-paper-scissors) vous donne l'implémentation dans plusieurs **dizaines** de langages de problèmes informatiques.
Ainsi, si vous voulez apprendre un nouveau langage, il peut être utile de lire ces **traductions** et d'essayer de faire le lien avec un langage que vous connaissez (Pour vous Python par exemple).

!!! note Activité
	Choisissez un des exemples du site et un langage que vous ne connaissez pas **hors ceux vus en cours** et essayer de trouver comment on y décrit **les constructions élémentaires**.
	Pour voir si vous avez compris, vous pouvez essayer de traduire le code python suivant dans le langage que vous avez choisi:

	```python
	debut = int(input())
	suite = debut
	longueur = 0
	
	while suite != 1
		if suite % 2 == 0:
			suite = suite * 2
		else:
			suite = 3 * suite + 1
		longueur = longueur + 1
	print("La longueur de la suite est:", longueur)
	```

### Exercices à faire chez soi

* Exercice 1,2,3 Bordas


## Séance 2: Spécification et Jeux de tests

Pour cette séance vous pouvez vous appuyez sur le chapitre $9$ du manuel NSI (Ellipses).

### Objectifs de la séance

Les objectifs du bulletin officiel sont:

| Notion                       | Commentaire                                                                   |
| ------                       | ----------                                                                    |
| Spécification                | Prototyper une fonction, décrire des pré-conditions et des post-conditions      |
| Mise au point de programme   | Utiliser des jeux de tests                                                    |
| Utilisation de bibliothèques | Lire la documentation et utiliser une bibliothèque pour résoudre un problème. |

### Déroulé

#### Prototype de fonction (Activité n°3 bordas)

!!! info À savoir
	Le **prototype** d'une fonction décrit:

	* Le nom de la fonction;
	* les valeurs attendues en entrée;
	* les valeurs attendues en sortie.
   
	Il est parfois appelé **Interface** et il permet d'utiliser la fonction **sans connaitre son fonctionnement interne**.
	De plus, grâce au prototype on peut facilement vérifier qu'une fonction **est bien implémentée** car il définit un **contrat d'exécution**.


#### Du cahier des charges à la documentation

!!! info Trace écrite
	Documenter un programme est l'une des phases les plus importantes, car **assez complexe** pour une machine.
	Une bonne documentation vous permettra de **mieux comprendre** *vos* programmes et *ceux des autres*.


#### Des jeux de tests

!!! info Trace écrite
	Les jeux de tests sont une phase incontournable de la programmation.
	Sans tests, votre code ne peut être correct, car rien ne le **montre**.
	
	En même temps, même avec des tests, votre code peut encore être incorrect.
	En effet, les tests sont **quasiment toujours** partiels.


## Pour aller plus loin

* [Une infographie complexe](https://www.info.ucl.ac.be/~pvr/paradigmsDIAGRAM.pdf) 

Quelques cours de collègues:

### Orientés python

* [Un autre cours de NSI](https://pgdg.frama.io/1nsi/langages/les_constructions/).
* [Un document de cours de 1ère](https://profjahier.github.io/docs/P6-1-constructions_elementaires.pdf) orienté sur la découverte des constructions.

### Une série de vidéo sur les différents paradigmes de programmation

1. [Histoire des langages](https://tube-sciences-technologies.apps.education.fr/w/3Kp8Kks3GHdFxX4kqeobxr)
2. [Langages impératifs](https://tube-sciences-technologies.apps.education.fr/w/fbm8Uv7CwQEJ1HZ6bjho6U)
3. [Langages fonctionnels](https://tube-sciences-technologies.apps.education.fr/w/sbg8WgExyTf8hhiD41XHCC)
4. [Programmation logique](https://tube-sciences-technologies.apps.education.fr/w/juAfWedsGBUqEaTVd2hafu)
<!-- 5 eme vidéo non disponible sur peertube -->
