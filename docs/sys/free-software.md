Nous avons vu qu'il y avait une distinction entre logiciels *libres* et *propriétaires*, nous allons ici détailler ce que cela veut dire.

!!! note "D'autres sources ?"
	Les ressources sur le logiciel libre et le **mouvement des communs numériques** sont légions.
	En voici quelques unes, pour étendre votre culture.
	
	* [le logiciel libre et son usage dans la recherche ](https://interstices.info/le-logiciel-libre-dans-la-recherche/)
	* [L'état soutien le logiciel libre](https://cnnumerique.fr/le-conseil-national-du-numerique-renforce-son-soutien-la-promotion-du-libre)
	* [Un article de la FSF](https://www.gnu.org/philosophy/free-sw.fr.html)
	* [Le libre ce n'est pas que le logiciel](https://framablog.org/2022/01/20/mais-ou-sont-les-livres-universitaires-open-source/) et tout les articles du framablog en général

# Logiciels Libres et Propriétaires

Les logiciels, comme toute oeuvre de l'esprit sont soumis au droit d'auteur et à la propriété intellectuelle.
Lorsque l'informatique a débuté, les programmes étaient surtout disponible sur papier et donc difficiles à partager.

Les logiciels étaient alors plus ou moins libre d'être échangés et étudiés.
Ils n'étaient pas forcément régis par une licence.

Aujourd'hui, on distingue fortement les licences des logiciels.
Il est plutôt commun de payer pour un logiciel dit *professionnel* et d'avoir des alternatives gratuites.
Nous allons étudier un mouvement spécifique, à la fois idéologique et très pratique, qui est celui du **Libre**.

## Le mouvement philosophique du Libre
### La création du mouvement

Avec l'avènement de la micro-informatique et la démocratisation de son accès, les enjeux financiers autour des logiciels deviennent importants.
On commence alors à breveter des logiciels, à les vendre et à ne plus diffuser les sources pour les étudier.

Ceci est en totale rupture avec l'ancien modèle, où les codes sources étaient fournis.
En effet, les machines étant très chères, elles étaient gardées longtemps et donc devaient être comprises pour être utilisées au mieux.

À partir des années 80 et de la portabilité promue par UNIX, ce ne sont plus les machines qui sont importantes, mais les logiciels.
La protection de leur savoir faire devient alors un facteur **essentiel** dans leur développement.

En 1984, [Richard Stallman](https://stallman.org) créa la [FSF](https://fsf.org) ainsi que le projet **GNU** pour développer un système libre.
Cependant, ce projet devait s'accompagner d'une licence libre elle aussi, pour promouvoir les valeurs que la FSF voulait porter.
La FSF créa donc la : **GPL**: General Public Licence, permettant de diffuser des logiciels librement en réglementant leur utilisation par les éditeurs propriétaires.

### Les 4 libertés

Pour qu'un logiciel soit considéré comme libre, il doit respecter ces 4 conditions, **sans exception**:


0. liberté d’exécuter le programme, pour tous les usages.
1. liberté d’étudier le fonctionnement du programme, et de le modifier pour l’adapter à ses besoins
2. liberté de redistribuer des copies.
3. liberté de redistribuer aux autres des copies de versions modifiées

On peut remarquer les libertés 1 et 2 impliquent l'accès au code source.

Contrairement au **domaine public**, la licence libre n'abandonnent pas les droits d'auteurs mais donne des permissions supplémentaires aux utilisateur·ices.



# À retenir

Dans les années 1980, les sociétés logicielles deviennent le moteur économique de l'industrie, à la place du matériel.
Le logiciel libre né d'une divergence d'opinion due à ce nouveau modèle économique.
La philosophie du libre décrit qu'un logiciel doit être: **exécutable**, **étudiable et modifiable**, **redistribuable**, **redistribuable après modifications**.
Ce n'est pas une cession des droits dans le domaine public, mais une ouverture des droits dans les limites acceptées par lae créateur·ices.
Le mouvement du libre s'oppose au propriétaire, qui retient les droits pour lui et empèche l'accès au logiciel.
Ce mouvement s'étend aujourd'hui à bien plus que le logiciel.
