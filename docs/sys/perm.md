
!!! info "Sources de ce cours"
	Ce cours est inspiré des sources suivantes:	

	* <https://iamjmm.ovh/NSI/permissions/site/index.html>
	* <https://eskool.gitlab.io/1nsi/os/linux/permissions>
	* <https://www.linuxpedia.fr/doku.php/droits_et_permissions_sous_linux>
	
	N'hésitez pas à aller les lire pour comprendre.

# Les droits et permissions d'accès
## Droits d'utilisation

Sous GNU/Linux, les fichiers peuvent être protégés des autres utilisateur·ices.
On parle de droits ou de permissions.

Le modèle des droits est assez simple, un fichier dispose de trois droits:

* **lecture**, pour obtenir le contenu du fichier, qu'on appelle aussi **Read**
* **écriture**, pour changer le contenu du fichier, qu'on appelle aussi **Write**
* **exécution**, pour exécuter le fichier comme programme, qu'on appelle aussi **eXecute**

### Représentation symbolique

Lors de l'affichage de ces droits, on adopte la convention d'écriture suivante:

* un `-` indique l'absence du droit
* une lettre indique la présence du droit en question (`r` pour **Read**, `w` pour **Write** et `x` pour **eXecute**.
* les lettres seront toujours (si présentes) dans l'ordre : `rwx`

Voici quelques exemples et les droits associés:

| Droits | Lecture | Écriture | Exécution |
| --     | --      | --       | --        |
| ---    | Non     | Non      | Non       |
| r--    | Oui     | Non      | Non       |
| -w-    | Non     | Oui      | Non       |
| --x    | Non     | Non      | Oui       |
| r-x    | Oui     | Non      | Oui       |

### Représentation octale

On voit dans le tableau précédent que les droits d'un fichier peuvent être codés sur **3 bits**, car les lettres sont toujours à la même place.

Par exemple, un fichier en lecture et écriture aura les droits `rw-` soit 110~2~ soit 6~10~.
Et un fichier sur lequel tout les droits sont donnés sera : `rwx` soit `111` soit `7`.

On dit que c'est la représentation octale d'un droit car on ne peut coder que 8 chiffres avec 3 bits, soit 8 combinaisons de droits.

## Propriétaire et comptes

Ces trois droits peuvent être donnés ou non à trois catégories d'utilisateur·ices dans le système:

* lae propriétaire du fichier, qui normalement aura tout les droits;
* le groupe propriétaires, qui contient plusieurs utilisateur·ices, dont lae propriétaire (parfois);
* les autres utilisateur·ices, qui ne font pas partie du groupe propriétaire.

Ainsi, on pourra donner les droits de lecture au groupe, mais pas d'écriture par exemple et refuser tout les droits aux autres.

Pour un fichier on aura donc toujours trois triplets de droits:

* les droits de lae propriétaire:
  * **Read** ? `r` ou `-`
  * **Write** ? `w` ou `-`
  * **eXecute** ? `x` ou `-`
* les droits du groupe propriétaire, sous la même forme que les précédents,
* les droits des autres, sous la même forme.

Pour représenter toutes les permissions d'un fichier on devra écrire les droits de ces trois catégories dans l'ordre.

Par exemple les droits suivants : `rwxr-w--x` se traduisent dans le tableau suivant:

| Représentation | `r`          | `w`          | `x`          | `r`     | `-`      | `w`       | `-`     | `-`      | `x`       |
| ---            | ---          | ---          | ---          | ---     | ---      | ---       | ---     | ---      | ---       |
| Présence       | Oui          | Oui          | Oui          | Oui     | Non      | Oui       | Non     | Non      | Oui       |
| Quoi           | Lecture      | Écriture     | Exécution    | Lecture | Écriture | Exécution | Lecture | Écriture | Exécution |
| Qui            | Propriétaire | Propriétaire | Propriétaire | Groupe  | Groupe   | Groupe    | Autres  | Autres   | Autres    |

On pourra aussi voir la représentation octale qui se traduit de la même façon: 7~8~ 5~8~ 1~8~ : 111~2~ 101~2~ 001~2~

| Représentation | 1            | 1            | 1            | 1       | 0        | 1         | 0       | 0        | 1         |
| ---            | ---          | ---          | ---          | ---     | ---      | ---       | ---     | ---      | ---       |
| Présence       | Oui          | Oui          | Oui          | Oui     | Non      | Oui       | Non     | Non      | Oui       |
| Quoi           | Lecture      | Écriture     | Exécution    | Lecture | Écriture | Exécution | Lecture | Écriture | Exécution |
| Qui            | Propriétaire | Propriétaire | Propriétaire | Groupe  | Groupe   | Groupe    | Autres  | Autres   | Autres    |


## Affichage en ligne de commande

Les droits des fichiers sont visualisables à l'aide de la commande `ls` avec l'option courte `-l`.

Ils sont alors présentés selon le format suivant:

```console
$ ls -l mon_dossier/ #Affichage des droits de tout les éléments de mon_dossier
drwxr-xr-x 5 user group  4096 12 oct.  21:35 docs
-rw-r--r-- 1 user group 19607  8 oct.  19:25 LICENSE
...

$ ls -l mon_dossier/LICENSE #Affiche les droits du fichier LICENSE dans le dossier mon_dossier
-rw-r--r-- 1 user group 19607  8 oct.  19:25 LICENSE
```

Ici, on lit les informations selon le format suivant:

* le premier caractère désigne le type du fichier : `d` pour dossier et `-` pour fichier;
* les trois caractères suivants désignent les droits de lae propriétaire;
* les trois suivants ceux du groupe;
* les trois derniers sont ceux des autres utilisateur·ices;
* la 3ème colonne est le nom de lae propriétaire;
* La 4ème est le nom du groupe.

Donc si on lit les droits suivants : `rw-r--r--` pour le fichier `LICENSE` on lit que:

* Lae propriétaire peut **lire**, **écrire** mais pas **exécuter** le fichier;
* Les membres du groupe peuvent **lire** uniquement;
* Les autres utilisateur·ices peuvent **lire** uniquement.

## Modification

On peut modifier les droits des fichiers pour donner des accès ou les enlever.
De même on peut modifier le propriétaire ou le groupe propriétaire.

### Des droits

Pour modifier les permissions d'un fichier ou d'un dossier, on utilise la commande `chmod`.
Celle ci permet aussi bien d'ajouter que d'enlever des droits à des fichiers ou dossiers.

!!! tip "Qui peut faire cela ?"
	Seul le propriétaire d'un fichier ou bien le super-utilisateur **root** peut modifier les droits d'un fichier.
	Selon les systèmes, les membres du groupe peuvent si ils ont le droit d'écrire (`w`).
	Selon les systèmes, les autres utilisatrices peuvent si elles ont le droit d'écrire (`w`).

#### En mode octal

Pour modifier les droits d'un fichier on peut utiliser `chmod` avec la syntaxe `chmod droits fichier`.

!!! tips "Exemple de modifications de droits"

	```console
	$ chmod 777 monfichier #Donne les droits à tout le monde sur mon fichier
	$ chmod 700 monfichier #Me donne tout les droits et aucun aux autres ni au groupe
	$ chmod 644 monfichier #Me donne les droits de lecture et écriture et seulement lecture au groupe et aux autres.
	```

#### En mode symbolique

On peut aussi utiliser la représentation symbolique vue plus haut, qu'on appelle aussi **UGOA**.
Dans ce cas là, on pourra **ajouter** et **enlever** des droits au fichier avec la syntaxe suivante: `chmod [ugoa][+-=][rwx]`.

Une petite explication de la syntaxe:

* On définit d'abord pour **qui** on veut modifier les droits en écrivant: 
  * `u` pour l'utilisateur,
  * `g` pour le groupe,
  * `o` pour les autres 
  * ou `a` pour tout le monde
* On peut faire des combinaisons `ug` veut dire `u` **ET** `g`

Ainsi la commande **incomplète** `chmod go...` demande une modification des droits pour le **G**roupe et les autres (**O**ther).

* On définit ensuite **comment** on veut modifier les droits:
  * `+` pour en ajouter
  * `-` pour en enlever
  * `=` pour les définir

Ainsi la commande **incomplète** `chmod go+...` demande un ajout de droits pour le **G**roupe et les autres (**O**ther).

Enfin, on définit **quoi**, les droits eux mêmes, en écrivant leurs lettres à la suite.
Ainsi la commande **complète** `chmod go+rw` demande un ajout des droits **lecture** et **écriture** pour le **G**roupe et les autres (**O**ther).

!!! tip "Modifier récursivement"
	Parfois on veut modifier les droits de tout un dossier et tout ses sous-dossiers et leurs sous-éléments.
	Par exemple enlever les droits de lecture et écriture à tout le monde sauf le propriétaire.
	
	Pour cela, on utilise l'option `-R` de chmod, qui va appliquer la modification de droits à **tout ce que contient le dossier et ses sous-dossiers**.
	Par exemple: `chmod -R go-rwx DOSSIER` permet d'enlever tout les droits au groupe sur DOSSIER et tout ce qu'il contient.
	
### Des propriétaires

!!! warning "Pas trop au programme"
	Les commandes suivantes ne sont pas au programme de NSI mais sont **très utiles**.
	
!!! tips "Qui peut faire cela ?"
	Seul le propriétaire ou **root** peuvent modifier le groupe et le propriétaire.

Les deux commandes ci dessous acceptent l'option `-R` pour modifier les propriétaires et groupes récursivement.

#### chown

Pour changer le propriétaire d'un fichier, on a la commande: `chown` qui prend deux arguments:

* le nouveau propriétaire
* le chemin vers le fichier

Ainsi `chown nadia monfichier` fait de nadia la propriétaire du fichier `monfichier` situé dans le répertoire courant.

#### chgrp

Pour changer le groupe d'un fichier, on a la commande: `chgrp` qui prend deux arguments:

* le nouveau groupe
* le chemin vers le fichier

Ainsi `chgrp eleves monfichier` fait du groupe `eleves` le groupe propriétaire du fichier `monfichier` situé dans le répertoire courant.

<!-- Définitions -->

*[UGOA]: User Group Other All
