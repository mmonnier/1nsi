!!! danger "Tout les systèmes"
	Il peut arriver que selon la version des systèmes, des outils ou des bibliothèques installées, certains codes ci dessous ne fonctionnent pas.
	Dans ce cas, l'utilisation du manuel `man` ou bien de l'aide intégrée sera un très bon exercice !
	
Nous allons ici étudier rapidement les commandes basiques des systèmes Unix et GNU/Linux.
Comme précisé, les codes ci dessous sont normalement universels sous ces systèmes, il peut y avoir cependant des variations mineures.

# Commandes GNU/Linux

Sous GNU/Linux, des centaines de commandes sont disponibles selon la distribution utilisée.
Nous n'allons pas ici étudier chacune de ses commandes en détails mais seulement les principales commandes pour:

* manipuler le système de fichier;
* se déplacer dedans;
* obtenir de l'aide sur les commandes;
* combiner les commandes;

## Un TP pour les manipuler toutes.

Pour apprendre en **s'amusant** , avant de faire le cours, nous vous proposons d'utiliser le logiciel *Gameshell* (<https://github.com/phyver/GameShell/blob/master/README-fr.md>) qui est un **jeu sérieux**.

## Préambule au TP

Gameshell n'est pas installé sur les ordinateurs, il faut donc le télécharger et le lancer pour pouvoir l'utiliser.
Pour cela, ouvrez un **Terminal** et tapez y **exactement** les deux lignes suivantes, en appuyant sur ++enter++ à la fin de chaque ligne.

!!! danger "Copier Coller"
	Dans un **Terminal**, les raccourcis clavier n'ont pas le même sens que dans une interface graphique, ainsi ++ctrl+c++ ne permet pas de coller.
	Vous devez donc bien taper ces commandes, ou bien utiliser votre souris (Clic droit, coller).

```bash
wget https://github.com/phyver/GameShell/releases/download/latest/gameshell.sh
bash gameshell.sh
```

!!! tip "Mais jusqu'à où ?"
	Votre objectif est au moins d'arriver au niveau 15 ! (Mais vous pouvez le dépasser bien entendu).

### Pour ne pas se perdre

Vous devrez compléter et prolonger le *plan* suivant:

```mermaid
graph TD
            A[Monde] -----> B[Chateau]
            A --> C[Echoppe]
            A --> D[Forêt]
            A ---> E[Jardin]
            A --> F[Montagne]
			
			B --> Bp[B.............]
			B --> Bc[C.....]
			B --> Bd[D........]
			B --> Bg[G.............]
			B --> Bo[O.............]
			
			Bp --> Bpb[B............]
			Bp --> Bps[S............]
			
			Bd --> Bd1[P............]
			
			E --> Ec[C........]
			E --> Ej[J..............]
			E --> El[..........]
			
			F --> Fg[G......]
```

Vous devrez aussi écrire les commandes que vous apprenez sous la forme suivante:

* Nom de la commande: `ls`
* Rôle de la commande: afficher le contenu du répertoire courant.
* Exemple: `ls` affiche `edt.txt NSI HGGSP` si le répertoire courant contient un fichier `edt.txt` et deux dossiers `NSI` et `HGGSP`.
* Variantes: `ls -l` pour obtenir plus d'informations et `ls chemin/vers/dossier/` pour afficher le contenu d'un autre répertoire que celui où on est.


## La syntaxe des commandes

Les commandes sont *presque toujours* formées selon le même modèle:

* un nom de programme, sensible à la casse (comme `echo`);
* des options de deux types:
  * les options courtes, commencées par un `-` et souvent formées d'une ou deux lettres maximums (comme `-v`;
  * les options longues, commencées par `--` et souvent formées d'un mot en anglais (comme `--usage`)
* des arguments, ou paramètres optionnels ou non.

Tout ces éléments sont séparés par des espaces.

!!! example "Une commande décortiquée"
	Dans la commande suivante : ` wc -l ~/.bashrc ~/.bash_aliases` on retrouve les éléments évoqués ci dessus.
	
	| Nom | Option courte | Arguments |
    | -- | -- | -- |
    |  `wc` pour *word count*  | `l` pour *lignes* | `~/.bashrc` et `~/.bash_aliases`, les fichiers à compter |
	
Il est très important lorsqu'on écrit une commande de faire attention à la **casse** des caractères.
La nom `Wc` ne désigne pas le même exécutable que `wc`, le premier n'existant d'ailleurs pas.

#### Combiner les commandes

Pour exécuter plusieurs commandes à la suite en une seule ligne, il est possible de les séparer par des `;`.
Ceci exécutera toutes les commandes à la suite, sans se soucier du résultat des précédentes.
Ainsi `com1 ; com2 ; com3` exécutera toujours `com1`, puis `com2`, puis `com3`, sans se soucier de la réussite ou de l'échec des commandes.

L'inconvénient de la virgule est que si une commande précédente échoue, cela peut entraîner l'échec des commandes suivantes.
Pour éviter cela, on peut remplacer la virgule par le mot `&&` qui signifie **et puis**.
Avec ce mot, la suite `com1 && com2 && com3` exécutera `com2` uniquement si `com1` a réussi, puis `com3` uniquement si les deux précédentes ont réussies.

### Lire des données


Pour donner des données en entrée à une commande, on a plusieurs solutions.
La plupart du temps, les commandes utilisées sans argument lisent **l'entrée standard**, c'est à dire ce qui est tapé au clavier.

On peut changer ce comportement avec la syntaxe `commande < fichier` qui va remplacer **l'entrée standard** par le contenu de fichier (comme si vous l'aviez tapé au clavier.

### Écrire des données

L'affichage d'une commande se fait normalement sur **la sortie standard**, si aucun argument ne change ce comportement.
La sortie standard correspond au terminal où vous tapez.

On peut changer ce comportement avec la syntaxe `commande > fichier` qui va écrire tout ce qu'affiche `commande` dans le fichier appelé `fichier`.

!!! warning "Écrasement des données"
	En utilisant `>`, le contenu du fichier est écrasé et donc effacé.
	Si on veut le conserver et écrire à la suite, on peut utiliser `>>` qui écrit à la fin du fichier.

### Enchaîner en lisant des données

On peut aussi faire une combinaison plus complexe, **le tuyau** (*pipe* en anglais) désignée par le caractère `|`.
Quand on exécute `com1 | com2`, la **sortie** de (tout ce qui est affiché par) `com1` est redirigée dans l'entrée de (ce qui est lu par) `com2`.
Cette dernière opération est plus complexe et nécessite un peu d'expérience, mais s'avère très utile lorsque l'on veut enchaîner les traitementssur des textes par exemples.

!!! example "Enchainer avec pipe"
	Les commandes `uniq`, `sort` et `wc -l` permettent respectivement de:
	
	* Supprimer les lignes en doublons d'un fichier qui se suivent
	* Trier les lignes d'un fichier ou de son entrée
	* Compter le nombre de ligne d'un fichier ou de son entrée
	
	Ainsi, la commande `sort fichier.txt | uniq | wc -l` permet de compter le nombre de lignes uniques dans un fichier.
	En effet, `sort fichier.txt` affiche le contenu du fichier trié, les lignes de même valeurs se suivent donc.
	La commande `uniq` supprime les lignes qui sont en doublons et se suivent et affiche le résultat, on n'a donc en sortie que les lignes uniques.
	La commande `wc -l` compte le nombre de lignes de l'entrée, donc le nombre de lignes unique du fichier.

## Les commandes de bases

Voici quelques tableaux récapitulant les principales commandes à connaitre, par catégories.

Les informations données ici sont succintes, pour avoir plus d'informations vous pouvez utiliser le manuel ( commande `man` ) ou l'option `--help` ou `--usage`.

### Le système de fichier

#### Les fichiers
| Commande | Description                                                              | Exemple                              |
| ---      | ---                                                                      | ---                                  |
| `ls`     | Affiche les fichiers du répertoire courant ou donné en argument          | `ls /etc/`                           |
| `cp`     | Copie un fichier dans un autre, ou des fichiers dans un répertoire       | `cp f1 f2`                           |
| `mv`     | Permet de déplacer un fichier en supprimant l'original et la destination | `mv f1 f2`                           |
| `rm`     | Permet de supprimer les fichiers données en argument                     | `rm f1 /etc/f2 /tmp/f3`              |
| `touch`  | Crée des fichiers vide                                                   | `touch monfichier /etc/autrefichier` |


#### Les répertoires

| Commande | Description                             | Exemple                                 |
| ---      | ---                                     | ---                                     |
| `cd`     | Permet de changer de répertoire courant | `cd /home/Camille`                      |
| `pwd`    | Permet d'affiche le répertoire courant  | `pwd`                                   |
| `mkdir`  | Crée un répertoire s'il n'existe pas    | `mkdir /tmp/nouveau` ou `mkdir nouveau` |
| `rmdir`  | Supprimer un répertoire s'il est vide   | `rmdir`                                 |

#### Les fichiers textes

| Commande | Description                                           | Exemple                      |
| ---      | ---                                                   | ---                          |
| `cat`    | Affiche les fichiers passés en argument               | `cat f1 ../f2 ../dossier/f3` |
| `nano`   | Un éditeur de texte pour les fichiers                 | `nano ~/NSI/pastouche.txt`   |
| `more`   | Comme cat, mais en paginé (petit à petit)             | `more unlongfichier`         |
| `grep`   | Permet de chercher un texte dans des fichiers         | `grep motif fichier`         |
| `uniq`   | Affiche un fichier sans les doublons s'ils se suivent | `uniq un_grand_fichier`      |
| `sort`   | Trie les lignes d'un fichier                          | `sort dictionnaire`          |
| `wc`     | Compte les lignes, les caractères d'un fichier        | `wc -l /etc/passwd`          |
| `head`   | Affiche le début d'un fichier                         | `head /var/log/messages`     |
| `tail`   | Affiche la fin d'un fichier                           | `tail /var/log/messages`     |
| `diff`   | Affiche les différences entre des fichiers            | `diff fichierV1 fichier_V2`  |

#### Diverses

| Commande | Description                              | Exemple                                 |
| ---      | ---                                      | ---                                     |
| `man`    | Affiche l'aide complète sur une commande | `man diff`                              |
| `kill`   | *Tue* un processus                       | `kill -9 id_d_un_long_processus_planté` |
| `ps`     | Affiche les processus en cours           | `ps`                                    |
| `top`    | Affiche les processus dynamiquement      | `top`                                   |
	
### Les droits d'accès

| Commande        | Description                                               | Exemple                                                                                                                        |
| ---             | ---                                                       | ---                                                                                                                            |
| `chmod`         | Permet de changer les permissions d'un fichier            | `chmod g+w /chemin/du/fichier`                                                                                                 |
| `chown`         | Permet de changer le propriétaire d'un fichier ou dossier | `chown nouveau_proprio monfichier`                                                                                             |
| `chgrp`         | Permet de changer le groupe d'un fichier                  | `chgrp nouveau_groupe monfichier`                                                                                                                             |
| `ls -l fichier` | Permet d'afficher les permissions d'un fichier            | `ls -l monfichier` donne les droits du fichier, `ls -l dossier` affiche les droits de tout les éléments du dossier en argument |
