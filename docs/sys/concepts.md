Nous allons ici étudier rapidement les concepts qui sous-tendent les systèmes d'exploitation.

![Le système d'exploitation se place à l'interface entre le matériel et les applications que vous utilisez tout les jours. Illustration de Golftheman.](../img/sys-exp.png){: style="height:320px;width=250px"}

Quel que soit votre système, il est toujours formé sur les mêmes bases théoriques.
On peut aussi parler des *fonctions* d'un système d'exploitation.

!!! note "OS / SE / Système"
	Nous utilisons ici indistinctement:
	* Le terme système
	* L'abréviation francophone SE
	* L'abréviation anglophone OS.
	
	Pour parler d'un système d'exploitation.

# Concepts principaux des systèmes

!!! info "Une petite vidéo"
	Pour celleux intéressé·es, une vidéo expliquant les principaux concepts: <https://dev.viewtube.io/watch?v=ECCr_KFl41E>

Un **système d'exploitation**, quel qu'il soit a plusieurs buts.
Le principal est de fournir aux utilisateur·ices un accès simplifié à la machine sous-jacente.

!!! note "Système et logiciels"
	Un système d'exploitation fourni à l'utilisateur et aux applications une vue simplifiée de l'ordinateur.
	Pour cela, le système est exécuté par la machine.
	Ainsi le système est un logiciel comme un autre, c'est même selon les cas **un ensemble de logiciels**.

## Une abstraction du matériel sous-jacent

Un système permet de facilement utiliser les différents composants du [modèle de von neumann](../von-neumann.md) c'est à dire:

* La mémoire vive
* La mémoire morte
* Le processeur
* Les périphériques d'entrée sortie.

L'OS gère ces ressources **physiques** via des abstractions:

* La mémoire virtuelle
* Les fichiers
* Les processus
* Les entrées sorties.

Ainsi, grâce au SE, l'application n'a pas à se soucier du matériel sur lequel elle est exécutée.
Tout ceci est uniformisé derrière une *interface commune* et simplifiée.

!!! note "Les systèmes *holistiques*"
	Auparavant, les systèmes d'exploitation étaient étroitement liés au matériel pour lequel ils étaient conçus.
	Aujourd'hui, nous avons complètement changé de paradigme.
	Les systèmes doivent gérer une grande diversité d'architecture, mais offrir toujours la même interface.
	Ils ne peuvent donc pas être conçus avec une machine spécifique sous-jacente.
	
	Cependant, certaines entreprises repartent en arrière, vers la création de systèmes dits *holistiques*.
	Ces systèmes sont faits pour être étroitement liés à une architecture spécifique, pour en tirer pleinement partie.
	Le problème avec cette méthode est la **non-portabilité**.
	Si le système est trop lié à un matériel donné, il en sera indissociable et donc moins utilisable qu'un système diffusé sur de nombreuses machines.

### Les interfaces du système

Pour les utilisateur·ices non spécialistes, l'interface la plus visible d'un système est **l'interface graphique**.
Cette interface permet d'utiliser la souris et le clavier pour interagir avec **le système de fichiers** et les **processus** en lançant des applications et déplaçant des icônes.

Pour les utilisateur·ices travaillant à plus bas niveau, on trouve **l'interface en ligne de commande**.
Celle ci est universelle et se base uniquement sur les textes.
Elle est plus puissante que l'interface graphique mais aussi plus complexe à utiliser.

Enfin, pour les programmes et les développeur·euses système, on trouve **l'interface programmative**.
On l'utilise lorsqu'on écrit des programmes et elle permet un accès complet au système.
C'est grâce à elle que l'on peut écrire des applications pour modifier des fichiers, afficher des images ou même communiquer sur le réseau.

C'est grâce à cette dernière interface que l'on peut directement communiquer avec **le noyau** du système.
Le noyau est le logiciel (ou l'ensemble de logiciels dans le cas des micro-noyaux) qui assure les fonctions essentielles d'abstraction du système.

![Une différenciation entre système mono et microlithiques, on voit où sont situées les différentes fonctionnalités](../img/OS-structure.svg)
### Des abstractions modulables.
## La division des ressources

Le système permet un accès facilité aux ressources matérielles.
Cependant celles-ci ne sont pas illimitées.

Pour éviter **la congestion** (l'utilisation trop forte) des ressources, le système est équipé de *stratégies de division*.
Celles ci sont invisibles pour les utilisateur·ices et les programmes, qui pensent avoir accès à l'entièreté des ressources de la machine, en permanence.

Ces stratégies de divisions permettent aux applications de fonctionner *comme si elles avaient accès à l'entièreté des ressources*.
Cela, alors même que plusieurs dizaines (centaines sur les serveurs) d'entre elles tournent en même temps sur la machine, avec les mêmes ressources à se partager.

### La division du temps d'exécution

Une des divisions les plus visibles, est celle du temps processeur.
Chaque coeur du processeur ne peut exécuter qu'une instruction à la fois.
Il est donc **impossible** sur un processeur **mono-coeur** d'exécuter plusieurs programmes en même temps.
Cependant, il semble que cela soit fait de nos jours, avec la multitude des applications que l'on lance.

Pour obtenir ce résultat, le système **ordonnance** les processus (les programmes en cours d'exécution).
C'est à dire que chaque programme est exécuté à tour de rôle pendant quelques microsecondes, puis on passe au suivant.
Le système donne ainsi l'illusion du parallélisme, même s'il ne l'est pas.

### La division de l'espace mémoire

Grâce à sa vision totale des ressources mémoire, le système peut tout utiliser.
Cependant, il a aussi la responsabilité de distribuer l'accès à cette mémoire aux applications.
Le but final étant que chaque application ait l'impression d'avoir accès à l'entièreté de la mémoire vive.

Pour cela, le système crée ce qu'on appelle **la mémoire virtuelle**.
Il laisse à l'application en cours d'exécution le droit d'accéder à *environ* toute la mémoire utilisateur.
Cependant, dès que cette application est *désordonnancée* (laisse la place à une autre pour son exécution), toutes ses données sont déchargées de la mémoire pour mettre à la place celle de la deuxième application.

Ainsi, les applications peuvent **tout utiliser**, au prix d'un temps de chargement légèrement plus long.

!!! note "Déchargement et rechargement"
	En vérité, toute la mémoire d'un programme n'est pas déchargée à chaque fois.
	On a plutôt une **recharge sélective**, si un programme s'attend à trouver des données à un emplacement précis, mais qu'un ancien programme y a écrit, le système déclenche un mécanisme spécifique pour charger les données du nouveau programme et enregistrer celles de l'ancien.

## La sécurité 

L'accès aux ressources matérielles peut occasionner des dégâts physiques (écrasement d'un disque dur, détérioration de la mémoire vive ...), mais aussi logiques (écrasement de données, corruption de l'exécution, accès à des données privées ...).
À cause de cela, le système doit protéger les ressources auxquelles il donne accès.

Cette protection se fait à plusieurs niveaux.
On a une protection par les rôles d'utilisateur·ices, qui ont chacun·e plus ou moins de privilèges d'accès.
On a ensuite une protection entre les différents programmes, pour éviter qu'ils ne corrompent ou ne volent des données ne leur appartenant pas.

Enfin, le système doit se protéger des autres programmes.
En effet, l'accès incontrôlé aux ressources logicielles du système permet d'en prendre le contrôle.
Le **noyau** en particulier, à accès à l'entièreté de l'espace mémoire et potentiellement du temps d'exécution.
Un programme mal intentionné qui serait chargé dans l'espace du noyau pourrait donc (très facilement) accéder à toutes les données (privées) des applications et monopoliser l'exécution.

C'est pour éviter ce genre de comportement que le système cloisonne très strictement les droits de chacun·es.

!!! warning "Linux est plus sécurisé que Windows !"
	On peut souvent entendre cet argument pour *sponsoriser* le passage à GNU/Linux ou MacOs par rapport à Windows.
	Il n'est pas vrai dans l'absolu, les deux systèmes ont des modes de sécurité similaires avec quelques différences notoires.
	Cette réputation de sécurité est surtout due au faible nombre d'utilisateur·ices de GNU/Linux.
	Les *hackers* n'ont donc que peu d'intérêt à attaquer ce système, là où le nombre important d'utilisateur·ices de Windows en fait une proie tentante.
	
	On trouve des virus spécifiques à GNU/Linux (ou non), aussi dangereux que ceux pour Windows.
	Cependant, du point de vue de la rapidité de correction, GNU/Linux est meilleur.
	En effet, grâce à la philosophie du Libre, les failles peuvent être rapidement détectées (grâce à des audits de sécurité) et colmatées puis distribuées.
	
# À retenir

...
