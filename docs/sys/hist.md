---
title: "Historique des systèmes d'exploitation"
author: "Marius Monnier"
---

!!! note "Références"
	Ce cours est inspiré de celui de <https://clogique.fr/nsi/premiere/linux.html> sur le système Linux ainsi que de celui des manuels bordas et hachette.

On a vu que plusieurs systèmes d'exploitations sont aujourd'hui largement répandus.
Nous allons nous intéresser à l'historique de ces systèmes.
Celui que nous utiliserons la plupart du temps en NSI est *GNU\/Linux*, dans la distribution Ubuntu.

# Historique: Des premiers systèmes UNIX et MULTICS à nos systèmes actuels

## MULTICS

En 1964, le premier système d'exploitation *grand public* MULTICS est proposé par le [MIT](https://web.mit.edu).
Il coïncide avec l'apparition des premiers *mini-ordinateurs* (en 1960), des machines moins puissantes que les *mainframes* mais surtout *moins chères*.

L'apparition de ces mini-ordinateurs a fait grandement augmenter la puissance de calcul disponible pour l'utilisateur.
Les ordinateurs ont ainsi été équipés de **spoolers** , logiciels capable de lancer des tâches (ou programmes) via une *file d'attente*.

!!! info "Multi-\*"
	Avant l'apparition des systèmes d'exploitation, les utilisateurs devaient eux même faire la file pour utiliser les ordinateurs.
	De plus, les ordinateurs ne pouvaient exécuter **qu'un seul programme** à la fois.

	On appelait cela les machines *mono-utilisateur* et *mono-tache*
	La première évolution a été de faire des machines multi-utilisateurs, que plusieurs personnes pouvaient utiliser avec leurs propres documents.

	Ensuite sont venues les machines multi-taches, où plusieurs programmes pouvaient s'éxecuter en même temps.
	De même, plusieurs utilisateurs étaient connectés en même temps via les **terminaux**.

MULTICS en lui même ne fut pas le système le plus utilisé car il était trop lourd et pas assez efficace.
Cependant, il propose des nouveautés qui pavent la route pour les futurs systèmes dits *de type UNIX*:

* Système de fichier hiérarchique et arborescent.
* Temps partagé (l'utilisation *simultanée* de la machine par les utilisateur·ices) **qui fut hérité de son prédécesseur CTSS**.
* L'invite de commande qui donne aujourd'hui le **Shell**. (hérité de CTSS)
* Traitement de texte. (hérité de CTSS)
* Les *anneaux de sécurité*, aujourd'hui connus comme *"mode noyau"* et *"mode normal"* surtout.
* Multi utilisateur.

!!! tip "Utilisation de MULTICS"
	Bien que MULTICS fut rapidement *oublié* dans le monde il fut beaucoup utilisé et distribué à Grenoble et en France en général
	En effet, la société Bull (aujourd'hui filiale de ATOS) était en possession des marchés universitaires et de MULTICS à partir des années 1980.
	
    Le dernier site MULTICS connu fut un ordinateur du Ministère de la Défense nationale du Canada, qui a été stoppé le 30 octobre 2000.
	Aujourd'hui, le code source de MULTICS a été publié par Bull : <https://web.mit.edu/multics-history/> et est hebergé par le MIT.

## UNIX

!!! note "L'histoire de UNIX en vidéo"
	Pour celleux intéressées, une vidéo expliquant les débuts de UNIX aux Bell Labs: <https://dev.viewtube.io/watch?v=ECCr_KFl41E>

En 1969, alors que MULTICS n'a pas tenu ses promesses d'efficacité, deux ingénieurs des laboratoires Bell décident de se lancer dans un nouveau système inspiré de MULTICS.

Ken Thompson et Dennis Ritchie créent ce système qu'ils nommeront d'abord UNICS puis finalement **UNIX**.

!!! note "Le nom UNICS"
	Le système fut nommé ainsi pour jouer avec le nom MULTICS, d'après Wikipédia:
	
	> Multi- car Multics faisait la même chose de plusieurs façons alors qu'Unics faisait chaque chose d'une seule façon

Il fut tout d'abord programmé en assembleur - le langage le plus bas niveau des machines - puis réécrit en [C](https://fr.wikipedia.org/wiki/C_(langage)).

!!! tldr "Le C"
	Le C est un langage créé pour le développement de UNIX et encore utilisé de nos jours pour la programmation système en général, par exemple pour développer le noyau Linux.
	Il est enseigné dans les classes préparatoires MP2I ainsi que dans les universités.
	
	Bien qu'ancien, il reste le langage de prédilection pour tout ce qui est bas niveau et gestion fine de la mémoire d'un programme.

UNIX a eu plusieurs dérivés qui sont encore aujourd'hui réputés comme **fiables** et très utilisés dans l'industrie comme:

* Linux
* Les systèmes **BSD**, dont dérive **MacOs**
* Des systèmes créés par IBM et HP

## MS-DOS et Windows

### À l'origine, CP/M

En parallèle du développement de UNIX, un autre système s'est développé, se voulant *grand public* et non industriel ou universitaire.
Il s'agit de **CP/M**, mis au point en 1973, ce fut le premier système non holistique, c'est à dire qui n'était pas fixé à un type de machine particulier, ce qui fit son succès.

Peu après, en 1981, IBM invente le concept de PC, sans être convaincue de son utilité.
IBM fera donc appel à Gary Kildall, le concepteur de **CP/M** pour utiliser son système qui était le plus répandu sur les micro-ordinateurs, ce que Kildall refusa.
IBM se tourne alors vers une société éditrice de logiciels de programmation en BASIC, **Microsoft**, pour développer son système.

### De QDOS à Windows

Microsoft ne créera pas de système mais rachètera un *clone expérimental* de **CP/M** nommé QDOS, qui deviendra DOS puis MS-DOS.

Par la suite, le succès de l'architecture PC est fulgurant et de multiples constructeurs se mettent à faire des ordinateurs **compatible PC**.
Étant donné l'appartenance de cette architecture à IBM, toutes ces machines compatibles étaient installées d'office avec la version de MS-DOS commercialisée par Microsoft, qui devient *le système standard et portable pour PC*.

Par la suite, entre 1993 et 2001, Microsoft propose une interface graphique minimale pour MS-DOS, nommée Windows.
Le système est par la suite renommé en Windows et devient le système d'exploitation le plus utilisé au monde.

Le système se répand par des démarches *agressives* de commercialisation comme la pré installation forcée ainsi que la difficulté de proposer l'installation d'autres systèmes.
Cependant, il est aussi développé pour l'ergonomie et la facilité d'utilisation.

## GNU

Avec la micro-informatique, les logiciels qui étaient auparavant échangés librement sont désormais payants et leur code source n'est plus accessible.

Ainsi, en 1984, Richard Stallman, chercheur au MIT, lance le projet GNU.
GNU a pour objectif de construire un système d'exploitation compatible avec UNIX, mais entièrement constitué de briques logicielles libres.

!!! warning "Richard Stallman"
	Bien que ses contributions au projet GNU et au logiciel libre ne puissent être édulcorées, Richard Stallman reste un personnage complexe.
	Premièrement, sa figure de *gourou du libre* n'est pas la meilleure pour représenter la diversité des utilisateur·ices.
	Ensuite, ses prises de positions extrêmes sont complexes à tenir aujourd'hui, notamment avec le mouvement Open Source qui prend de plus en plus d'ampleur.
	
	Enfin, certains de ses propos sont notoirement discriminants, notamment dans l'affaire Hepstein.
	Une partie de la communauté l'érige comme *figure du libre*, ce qui est génant dans le sens où, d'après eux, s'il disparaissait le monde du Libre perdrait son moteur.
	Cette affirmation est très dangereuse car fait reposer toute une communauté et un mouvement sur une seule personne.
	
## Linux

En 1991, un étudiant finlandais, Linus Torvalds, crée un clone libre du noyau de UNIX, qui prendra le nom de Linux.

Ce noyau constitue le coeur du système d'exploitation.
C'est lui qui est en charge de la communication bas niveau entre le matériel et tout le reste du système.
C'est aussi à ce niveau que se fait l'interface avec les systèmes de fichiers, les périphériques et la sécurité.

C'est Linux, le noyau, qui associé à l'environnement GNU, les logiciels et interfaces, donnera le système d'exploitation **GNU\/Linux**.
Aujourd'hui, par paresse on abrège souvent le nom en *Linux*, alors que cela est encore plus faux avec la multitude de **distributions** qui existent.
En effet, sur le système de base GNU\/Linux se sont construits une multitude d'autres systèmes, que l'on nomme **distributions**, car elles empaquettent:

* Un noyau: Linux
* Un ensemble d'utilitaires de base: GNU
* Des interfaces graphiques
* Des ensembles de logiciels spécifiques
* Des ensembles de logiciels grands publics
* Diverses modifications au noyau et aux utilitaires GNU.

Chaque distribution répond à des besoins particuliers, pour les utilisateur·ices scientifiques, pour les graphistes, pour les entreprises, pour pour les particulier·es et les serveurs.
On retrouve même aujourd'hui GNU\/Linux sur les téléphones portables, avec le système Android et le projet AOSP.

Si les systèmes GNU\/Linux étaient au début réservés aux initié·es, ils sont aujourd'hui grandement simplifiés.
Le projet Debian, fondé en 1993 par Debra Lynn et Ian Murdock, est l'une des distributions GNU\/Linux les plus utilisées.
Elle sert de base entre autre aux distributions Ubuntu et dérivées, qui sont très accessibles aux utilisateur·ices novices.

Aujourd'hui, les distributions GNU\/Linux permettent de s'acquitter de toutes les taches usuelles: Bureautique, Internet, Art, Programmation ...

## MacOs

**À développer**

MacOs est un autre système dérivé des branches **BSD** de UNIX.
Il est principalement utilisé sur les ordinateurs de marque Apple.
Il a aussi dérivé en iOS, le système d'exploitation des téléphones Apple.

MacOs, de part sa parenté avec UNIX est assez proche de l'expérience d'un système GNU\/Linux.
C'est au niveau de son interface et de son caractère propriétaire que la plupart des différences se font.

