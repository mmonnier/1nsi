!!! info "Besoin de plus ?"
	Le site Zeste de Savoir propose plusieurs contenus de bonne qualité.
	Il y a entre autres [un tutoriel](https://zestedesavoir.com/tutoriels/1114/comprendre-les-encodages/) sur les encodages.
	N'hésitez pas à le lire pour mieux comprendre !


# Représentation des caractères

On a vu qu'en informatique, tout est codé par des **chaînes de bits**.
Les entiers naturels $`\mathbb{N}`$ sont ainsi stockées via leur **décomposition en base 2**.

On peut alors légitimement se dire que **les caractères** et les **textes** en général, seront eux aussi codés par des **chaînes de bits**.

Nous allons ici étudier différentes manières **d'encoder** les textes, qui ont été et sont toujours utilisées.

## La préhistoire des textes: ASCII / ASCII Étendu
### Des codages analogiques: Morse / Schappe

Avant d'utiliser l'informatique, ce n'était pas les **bits** qui étaient utilisés, mais la vision ou bien les signaux électriques.
Ainsi, en France, le [télégraphe de Chappe](https://fr.wikipedia.org/wiki/T%C3%A9l%C3%A9graphe_Chappe) mis au point en **1794** permettait de communiquer sur **plusieurs centaines de kilomètres** grâce à des signaux visuels.
En plus d'être déployés dans toute la France et de permettre des communications rapides (quelques minutes pour transmettre un message à Paris).

!!! note "Devoir ?"
	En allant sur Wikipédia et en visionnant la vidéo suivante [Histoire de l'informatique](https://videos.univ-grenoble-alpes.fr/video/17212-histoire-de-linformatique-reseaux/) à partir de 11 minutes.
	Vous aurez une très bonne vision de ce système.

Le télégraphe de Morse plus tardif a lui utilisé un codage binaire bien connu, à base de bips courts et longs ainsi que de <!-- pause -->.

!!! note "Devoir ?"
    Comparez le télégraphe de morse et ses avantages et inconvénient par rapport à celui de Chappe.


### Mais que veut on encoder ?

Lorsque l'on commence à coder du texte, les premières choses que l'on veut coder sont **les lettres**.
En français notre alphabet est constitué de **26 caractères** `A` à `Z`.
On doit donc attribuer une chaîne de bits à chaque caractères pour les coder.

Cette attribution **est arbitraire**, on pourrait par exemple décider de coder les lettres selon leur forme.
Cela amène par contre beaucoup de questions:

* Comment stocker une forme en binaire ?
* Les lettres ont-elles une forme *standard* ?
* Comment gérer les changements d'échelles ?
* Et les majuscules et minuscules ?

On va donc plutôt encoder les lettres (et tout les caractères en fait) selon un **ordre arbitraire**.

!!! note "À retenir"
	Tout les systèmes d'encodage sont arbitrairement décidés par des humains.
	Un système n'est donc pas **meilleur** qu'un autre dans l'absolu.
	Cependant certains systèmes sont plus adaptés que d'autres.

### Le codage des enfants: A=1,Z=26

Un premier ordre est celui de la position dans l'alphabet.

Comme *divulgâché* dans le texte de titre, on attribue donc un numéro à chaque lettre.
On commence avec $`1`$ pour `A` et on finit donc à $`26`$ pour `Z`.

??? note "Question: Combien vaut `M` dans cet encodage ? et `q` ?"
	`M` vaut 13, car c'est la 13^ème^ lettre de l'alphabet.
	`q` n'est pas défini, par contre `Q` l'est.


Ce codage est celui des enfants et celui que l'on imagine le plus facilement.
Par contre, comme vu il ne permet que de faire les majuscules ou les minuscules.

Il n'est donc **pas adapté** pour envoyer des textes contenant:

* Des caractères de **casse** différente
* Des signes de ponctuation
* Des chiffres
* Des émojis.

Il faut donc penser à ajouter dans notre **encodage** tout les signes de ponctuation nécessaires.

Quels chaînes de bits leur attribuer ?
Selon l'époque cela a varié et comme c'est **arbitraire** on peut faire ce que l'on veut.


### Un codage américain: ASCII 7bits + 1bit parité

Avant les années 1960, plusieurs systèmes de codages coexistent sans standardisation.
L'organisation Internationale de standardisation (**ISO**) a donc décidé de créer un standard en 1960 pour réguler et ordonner les communications mondiales.

En 1963 est alors dévoilée la première version de ASCII pour *American Standard Code for Information Interchange*.

!!! info "ASCII"
	Norme de codage américaine définissant:
   
   * 128 caractères différents
   * Codés sur 7 bits de $`0000000_2`$ à $`1111111_2`$
	Cette norme permet d'encoder l'alphabet américain, la ponctuation classique, les nombres et divers **signes de contrôles**
	

!!! note "Devoir ?"
	ASCII est fortement structuré.
	En vous aidant d'une table donnez les valeurs limites des différentes **classes de caractères**.
	Les majuscules et les minuscules sont placées de manière très spécifique.
	Trouvez la relation mathématiques permettant de passer de la représentation binaire d'une majuscule à celle d'une minuscule et inversement.

ASCII n'était pas codé sur 8 bits car à l'époque, l'économie en mémoire était importante.
Ce 8^ème^ bit n'était toutefois pas inutilisé car il pouvait servir à limiter les erreurs.

!!! info "Limitations de ASCII"
	Ne stockant que l'alphabet anglais, il est impossible d'utiliser ASCII uniquement pour transcrire toutes les langues du monde.
	De plus, n'avoir que 128 caractères maximum est bien trop peu pour tout les caractères que l'on utilise **même en anglais**.
	
Il a donc fallu imaginer des **extensions** à ASCII pour que chaque pays puisse transcrire ses propres caractères.

#### L'extension mondiale: On a 8 bits, utilisons 8 bits

Le codage ASCII est un standard reconnu dans le monde entier.

Cependant il est loin d'être suffisant. 

En effet:

* il ne contient aucun caractère accentué. 
* Les langues avec un alphabet différent (cyrilliques, kanji, etc.) ne sont pas présentes.
* Il n'est pas extensible.

Dans les années 70, la fiabilité des ordinateurs s'est accrue.
De même, la fiabilité des liaisons réseau a beaucoup augmentée.
On a donc commencé à utiliser le 8ème bit pour coder des caractères supplémentaires. 
Cela a conduit à tout un tas de standard différent, répondant à des usages.

On a eu des codages qui dépendaient :

* de la langue (avec des codages régionaux).
* du système d'exploitation utilisé.

Tout ces systèmes locaux survivent encore aujourd'hui.
On peut les rencontrer sur internet avec les problèmes d'encodage sur les sites mal faits par exemple.

!!! note "Devoir ?"
	Expliquez en quoi un jeu de caractère régional est pratique dans un pays mais inefficace à l'international.

## L'histoire mondiale: UTF-8/16/32
### Un standard pour les unifier tous: Unicode

Aujourd'hui, le standard utilisé est **Unicode**.
Ce système d'encodage **à taille variable** permet de représenter **tout les caractères mondiaux**.

Pour voir tout les caractères encodables : [visitez le site](https://home.unicode.org/).
On utilise aujourd'hui surtout le format **UTF-8**, qui permet de coder de manière compacte les caractères.
Cependant, il nécessite de traiter les données plus difficilement qu'un codage à taille fixe.

!!! note "Codage à taille fixe"
	Tout les caractères sont stockés **sur le même nombre de bits**
	
!!! note "Codage à taille variable"
	Certains caractères sont stockés sur plus de bits que d'autres.
    Comme l'unité est aujourd'hui **l'octet** il faut donc un moyen de **délimiter** les caractères sur plusieurs octets.
	

Pour représenter un caractère Unicode, on utilise la notation **hexadécimale**.
Chaque caractère a alors un code unique de la forme: `U+nombre hexadécimal`

En UTF-8, les caractères sont codés avec 1,2,3 ou 4 octets.

!!! info "Autres UTF ?"
	Il existe différents formats UTF:
	* UTF8: Le plus utilisé, les caractères sont stockés sur 1 à 4 octets.
	* UTF16: Utilisé par Microsoft Windows et le langage Java
	* UTF32: Utilisé par les systèmes Unix.

Les plus importants (*discutable*) sont UTF-8 et 32, car UTF32 est un codage à taille fixe, bien qu'il prenne beaucoup de place.
UTF-8 lui est **le standard** à utiliser.


En UTF-8 les caractères sont codés selon la norme suivante:

| Représentation binaire UTF-8        | Signification                |
| --                                  | --                           |
| 0xxxxxxx                            | 1 octet codant 1 à 7 bits    |
| 110xxxxx 10xxxxxx                   | 2 octets codant 8 à 11 bits  |
| 1110xxxx 10xxxxxx 10xxxxxx          | 3 octets codant 12 à 16 bits |
| 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx | 4 octets codant 17 à 21 bits |

On observe que le premier octet perd de *1 à 5 bits* pour indiquer le nombre d'octets codant le caractère.

Les octets suivants commencent alors tous par $`10_2`$.

**Aucun caractère Unicode sur 1 bit ne peut donc commencer par $`10_2`$.**

# À retenir jusqu'ici:


* Pour pouvoir stocker des caractères, on adopte un codage
* Différents codages ont coexisté localement et internationalement depuis 1700.
* Le code ASCII stocke les caractères sur un octet. Il est normalisé pour 128 caractères
* Les codes ASCII étendu utilisent les 8 bits. Ils varient suivant les systèmes et les régions (cp-1252,ISO-8859-1,etc.)
* Le codage UTF-8 permet de stocker tous les caractères de toutes les écritures
* UTF-8 est compatible ASCII
* En UTF-8, un caractère peut être stocké sur 1 à 4 octet
