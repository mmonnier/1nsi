
!!! info "Cours total"
	La version totale de ce cours est celle ci : <https://eskool.gitlab.io/1nsi/donnees/booleens/> , le point de vue mathématique n'a pas été abordé.

Pour le TP correspondant il est : [ici en pdf](./TP.pdf)

# L'algèbre de Boole

L'algèbre des booléens est nommé selon son concepteur Georges Boole, en 1847.
Il invente ce formalisme sans avoir la notion de ce qu'est un ordinateur à l'époque, bien que des machines mécaniques de calcul aient déjà été conçues.

Ce formalisme est aujourd'hui utilisé pour décrire le fonctionnement d'un ordinateur, dont les composants internes ne connaissent que deux valeurs: Vrai et Faux, Allumé ou Éteint.

## Les valeurs

Les deux seules valeurs de cet algèbre sont $`\top`$ pour Vrai (aussi appelé 1 ou *Top*) et $`\bot`$ pour Faux (aussi appelé 0 ou *Bottom*).
L'algèbre décrit les opérations entre ces élèments par 3 opérateurs principaux.

## Les opérateurs

On trouve tout d'abord l'opérateur de Conjonction : *ET* ou *AND* en anglais, qui associe deux booléens.
Cet opérateur est vrai dès lors que ces deux opérandes sont vraies, comme dans la phrase `Il fait beau et il fait chaud`, qui n'est vrai que si le soleil brille ET la température agrèable.

On trouve ensuite l'opérateur de Disjonction : *OU* ou *OR* en anglais, qui associe deux booléens.
Contrairement au *ou* francophone qui est un *ou bien*, le ou logique est vrai dès qu'une opérande est vrai et l'est aussi si les deux le sont.
C'est à dire que la phrase *Fromage OU dessert* en français veut dire *Fromage OU BIEN dessert (mais pas les deux)* alors qu'en logique elle veut dire *Fromage OU dessert (les deux sont possibles).

Enfin on trouve l'opérateur de négation : *NON* ou *NOT* en anglais, avec un seul argument.
Cet opérateur est plus simple, car il ne fait qu'inverser son argument, ainsi : *NON il fait beau* devient *Il ne fait PAS beau*.

On peut avec ces quatres opérateurs former toutes les expressions booléennes que l'on souhaite, on dit que cet ensemble est *complet*.
On verra qu'il y a d'autres ensembles d'opérateurs complets (comme le *NAND*).

## Les tables de vérités

Une table de vérité représente exhaustivement toutes les entrées et sorties d'une expression booléennes sous forme d'un tableau d'entrées et de sorties booléennes.


# Utilisation en informatique

## Les tests dans les programmes

## Les circuits logiques

# Un simulateur logique de circuits

Voici un simulateur logique que l'on utilisera pour modéliser des circuits électroniques:

<script src="https://logic.modulo-info.ch/simulator/lib/bundle.js"></script>

<div style="width: 100%; height: 430px">
	<logic-editor mode="design" showonly="in,out,and,or,not"></logic-editor>
</div>

## Des circuits à tester

<div style="width: 100%; height: 430px">
	<logic-editor mode="tryout" data="N4KABGBEBukFxgMwBpxQJYDt5gNrEgAcB7AZx1wCYAOABmTEoBZaBdBydAExwEYBODtACGAGz4BfBgRLkEVOg16I2HbjkoA2IWMnSiZCjXpgA7Kow8ElUzvEJeE9mkgBzYQBcApnLxoIBB4AnoReOJAAggByACKQHLIUTACsJrws7BjY8pQmlLyZkMQArh4alFL+YIEhYQiRsfFQifLJqQx0hVhGKIxMhSVl1smVEAGQwaHh0XEJhvKa5gz8FpzZeDQMlPwDpTgqo2M1U-UzTQa+uEyaeSpd67iIvAyIlLtDSIiH45N1UFEAeQAKucWnhkiwtnc1OtEEwOIN9iM0M4IEU9vIZPM8KZIWAVl0rEhNE5UGiAO7oABOPgouAEWwKDCo2kYFiotkYiEyVEoW36zMoyS21B5DKQb0FnKePMQvThsuFjB2zJUL00rFYIAkQA"></logic-editor>
</div>
