from flask import Flask, request, render_template, session, abort

import secrets

import modele as m

app = Flask(__name__)
app.secret_key= secrets.token_urlsafe(64)

@app.route('/')
def index():
    """
    Affiche la page d'accueil avec un message de bienvenue.
    """
    return render_template("index.html")

@app.route('/new_contact', methods=['GET'])
def add_contact():
    """
    Affiche le formulaire d'ajout de contact.
    """
    return render_template("add_contact.html")

@app.route('/contact/add', methods=['POST'])
def add_contact_handler():
    """
    Traite la soumission du formulaire d'ajout de contact et met à jour l'annuaire interne.
    """
    nom = request.form['nom']
    prenom =request.form['prenom']
    email = request.form['email']

    # Si on avait pas de contact on crée un annuaire vide.
    annuaire = session.get('annuaire',m.creer_annuaire())
    contact = m.creer_contact(nom,prenom,email)
    
    m.ajoute_contact(contact,annuaire):
    # On met à jour la session avec le nouvel annuaire
    session['annuaire'] = annuaire
    # On redirige vers la page d'affichage.
    return redirect(url_for('view_contact',email))
    

@app.route('/contact/search', method=['GET'])
def search_contact():
    """
    Affiche un formulaire de recherche de contact.
    """
    # Traitement de la recherche et affichage des résultats
    pass

@app.route('/contact/view/<email>')
def view_contact(email):
    """
    Affiche les détails d'un contact spécifique identifié par son email.
    """
    # Affichage des détails d'un contact spécifique identifié par email
    annuaire = session.get('annuaire',m.creer_annuaire())

    contact = m.trouve_contact(annuaire,email)
    if contact is None:
        abort(404)

    return render_template('view_contact.html',contact=contact,m=m)

@app.route('/contacts')
def all_contacts():
    """
    Affiche tous les contacts enregistrés.
    """
    # Affichage de la liste de tous les contacts enregistrés
    annuaire = session.get('annuaire',m.creer_annuaire())

    return render_template('all_contacts.html', contacts=annuaire,m=m)

if __name__ == '__main__':
    app.run()
