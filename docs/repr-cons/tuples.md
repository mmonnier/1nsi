# Les p-uplets

!!! warning "Tuples/p-uplets/n-uplets"
	Dans ce cours, on utilise indistinctement les mots *tuples*, *p-uplets* ou *n-uplets* pour parler des collections de valeurs non modifiable.
	On retrouve les trois termes dans la littérature anglaise et française.
	Ce cours est fortement inspiré de [Un zeste de python](https://zestedesavoir.com/tutoriels/2514/un-zeste-de-python/4-types/4-tuples/), disponible sur le site *Zeste de Savoir*.

On doit dans certaines situations stocker des informations relatives à un même objet, de différents types.
Par exemple, si on veut représenter une inscription de personne on devra stocker:

* Son nom
* Son âge
* Son mail

Avec des variables simples, on devrait avoir 3 variables par personne qui s'inscrit, ou bien trois tableaux distincts.
Ceci n'est pas simple à synchroniser, ni à manipuler ensuite.

À la place, on va utiliser des **p-uplets** ou **tuples**, qui permettent de stocker une **collection non mutable de valeurs de types différents**.
Tout comme les tableaux, les p-uplets sont:

* Ordonnés (les éléments ne sont pas placés aléatoirement dedans)
* Indiçables (on peut utiliser un indice pour voir un élément spécifique)

Cependant, **ils ne sont pas mutables**, une fois qu'un p-uplet est créé, on ne peut pas modifier ses éléments internes.

Ainsi, contrairement à un tableau l'instruction `tuple[0] = "blabla"` fera une erreur à l'exécution.

## Création

En python comme en algorithmique, on créera des p-uplets avec la syntaxe suivante:

```python
mon_tuple = ("Andréa", 15, "andrea@mail.fr")
```

On délimite donc les éléments avec des `()` autour et des `,` pour les séparer.

!!! note "couples, triplet ..."
	Pour les tuples les plus petits, on utilisera les noms suivants:
	
	* couple pour un tuple de 2 éléments : `("nom", 45)`
	* triplet pour un tuple à 3 éléments
    * quadruplet pour ceux à 4 éléments
	* quintuplets pour ceux à 5 éléments.

??? note "Question: Comment représenter un point dans un repère en python ?"
    Un point est un couple de coordonnées, on peut donc le représenter par le tuple suivant:
	
	```python
	point = ( coordonnee_x, coordonnee_y)
	```
	
??? note "Question: dans un bulletin, comment représenter une matière ?"
	On sait que dans un bulletin on ramène une matière à plusieurs données:
	
	* la note maximale
	* la note minimale
	* la moyenne générale
	* l'appréciation globale
	
	On pourra donc représenter une matière par un quadruplet:
	
	```python
	matiere = ( note_max,
		note_min,
		moyenne,
		appreciation 
		)
	```

### Tuples particuliers

#### Le tuple vide

Que représente l'expression python suivante : `python ()` ?
On peut utiliser la console python pour le savoir.

```python
>>> a = ()
>>> print(a)
()
>>> type(a)
<class 'tuple'>
>>> len(a)
0

```

On voit ici que cet expression `()` n'est pas un simple *parenthèsage* mais bien un tuple (d'après son type).
On voit ici que sa longueur est 0, donc c'est un **tuple qui ne contient rien**, c'est bien *le tuple vide*.

#### Les 1-uplet

On a des tuples de longueur quelconque et même le tuple vide.
Alors pourquoi pas des tuples de longueur 1 ? Des **1-uplet**.

D'après la syntaxe vue jusqu'ici, on devrait écrire: `("coucou")` pour désigner le 1-uplet ne contenant que le texte coucou.
Si on tape cela dans une console python on obtient:

```python
>>> a = ("coucou")
>>> print(a)
coucou
>>> type(a)
<class 'str'>
>>> len(a)
6
```

On voit que *a* n'est pas un tuple (son type est `str`) et qu'il a une longueur de 6, c'est donc un texte.
En effet, en python les parenthèses ont deux significations:

* La délimitation des tuples à 0 ou plus de deux éléments.
* La délimitation d'expressions pour changer l'ordre d'évaluation.

On a donc une ambiguïté d'interprétation lorsque l'on décrit *un tuple à un seul élément*.
Pour éviter cette ambiguïté, Python propose la syntaxe suivante: `(élément,)`.
On ajoute **une virgule** après l'unique élément, pour signifier qu'on a bien un tuple.

On peut vérifier cela dans la console python:


```python
>>> a = ("coucou",)
>>> print(a)
('coucou',)
>>> type(a)
<class 'tuple'>
>>> len(a)
1
```

## Opérations sur les tuples

On a dit qu'un tuple était une collection **indiçable, non modifiable**.
On peut donc accéder aux élèments du tuple grâce à des indices *comme pour un tableau*.

Pour accéder aux élèments, on se souviendra qu'ils sont numérotés dans l'ordre et que le premier **est à l'indice 0**.
Ainsi, pour voir le **second élément** d'un p-uplet avec $`n \geq 2`$ on devra utiliser l'indice 1.

On pourra vérifier que le code suivant donne bien les résultats décrits:

```python
>>> mon_triplet = (12,"NSI",0.5) #Création du triplet
>>> print(mon_triplet[0]) #Premier élément
12
>>> print(mon_triplet[2]) #Dernier élément
0.5
>>> mon_triplet[3] #Accès hors des bornes
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
IndexError: tuple index out of range
```

---

Comme on ne peut pas modifier les éléments d'un tuple le code suivant doit produire l'erreur attendue:

```python
>>> mon_triplet[0] = 13
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'tuple' object does not support item assignment
```

# À retenir

* Un tuple permet de stocker **une collection statique** de valeurs.
* Un tuple est **indiçable** : `tuple[i]` donne l'élément à l'indice `i` .
* Le premier élément d'un tuple est situé à l'indice 0.
* On peut obtenir la longueur d'un tuple avec `len`.
* Les tuples sont utiles pour stocker des **structures simples** où les valeurs sont liées.
