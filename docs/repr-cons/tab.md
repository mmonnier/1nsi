# Les tableaux

## Une vue formelle

Les tableaux sont une structure de données linéaire permettant de stocker différentes valeurs d'un même type.
Contrairement aux *tuples/p-uplets* les tableaux sont **mutables**, c'est à dire que l'on peut modifier les valeurs qu'ils contiennent.

On utilise souvent les tableaux pour représenter des suites de valeurs (suites mathématiques, séquences d'informations ...).

Habituellement, les tableaux sont vus comme des espaces de **taille fixe**.
On peut modifier leurs éléments internes, mais pas leur taille, ni où ils sont en mémoire.

On va voir que cette contrainte est relaxée en python, le langage utilisé en NSI.

### Utilisation d'un tableau
#### Indexation et affectation

Dans la plupart des langages **et en algorithmie** , on considérera que on accède aux éléments d'un tableau via des **indices entiers**.
Ainsi, si on a un tableau `T` contenant dans l'ordre les éléments `10,20,30,40`, on accédera au premier élément via la syntaxe: `T[0]`.

!!! note "Le premier c'est 0 ?"
	Cette convention de commencer à compter à partir de 0 vient à la fois des mathématiques et de l'architecture.
	En mathématiques, on distingue les nombres **ordinaux** (premier,deuxième,...) et les nombres **cardinaux** (Zéro,Un,deux,...)

	Les nombres ordinaux représentent l'ordre des éléments alors que les cardinaux représentent les quantités.
	On voit donc que si on suit l'ordre on peut assimiler premier et zéro, à peu près.

	En architecture des machines, on explique la syntaxe `T[0]` comme *"Un déplacement de 0 éléments depuis le début de T"*, soit le premier élément de T.

L'élément suivant sera donc `T[1]` et ainsi de suite ..

Les tableaux permettent donc de stocker plusieurs variables, de même type, relatives ou non.
Un cas classique d'usage du tableau consiste à stocker des données qui se suivent.
Par exemple, on pourra utiliser un tableau pour stocker les relevés de température d'une station météorologique.

Ce sont des éléments **comme les autres**, c'est à dire que l'on peut les stocker dans des variables : `mon_tableau = [10,20,30,42]`.
La seule différence avec une variable qui contiendrait un entier ou un réel est **l'indexation** vue précédemment.

De plus, les tableaux sont **mutables**, c'est à dire qu'on peut les modifier (via l'indexation) sans les recréer entièrement.
C'est grâce à cela que l'on peut écrire `T[3] = 0` pour modifier le 4^ème^ élément, sans toucher aux autres.

## Une vue pythonesque

En Python, les tableaux sont représentés par le type `list`.
Ce type permet plus d'opérations que celles normalement attribuées aux tableaux.

On pourra dynamiquement modifier leur taille, supprimer des éléments et en ajouter des nouveaux.
Nous assimilerons les deux types cette année, mais en Terminale vous verrez qu'ils ont deux utilités bien différentes.

### Utilisation des tableaux
#### Opérations de tableaux

En Python, pour créer un tableau à partir de rien, on écrira:

```python
mon_tableau = [34,56,78]
```

On dit qu'on le déclare par **extension**, car on décrit chaque élément du tableau.
Cette méthode devient tout de suite complexe quand on a des tableaux de 100,1000 ou 10000 éléments (imaginez un tableau représentant les âges des personnes en France).

On peut déclarer un tableau vide en précisant qu'il ne contient aucun élément: `tableau_vide = []`.

On va utiliser la **concaténation** et la **multiplication** de tableaux pour créer des tableaux plus grands plus facilement.

*Concaténation*:
	Concaténer deux tableaux, consiste à en construire un nouveau qui sera composée des éléments des deux, dans l'ordre.
	Ainsi si on a `T1 = [1,2,3]` et `T2 = [6,8,9]`, la concaténation de `T1` et `T2` sera le tableau `[1,2,3,6,8,9]`.
	En python, on écrit cette opération : `T1 + T2`.
	Attention, la concaténation n'est pas **symétrique**, c'est à dire que `T1 + T2` n'est pas le même tableau que `T2 + T1`.
	
Ceci permet facilement de créer un tableau qui soit l'ancien **et un nouvel élément**, en écrivant : `T = T + [element]`.

*Multiplication*:
	Comme pour les entiers, la multiplication est juste une addition répétée.
	Ainsi, au lieu d'écrire `T1 + T1 + T1` on écrira en python : `T1 * 3` pour créer un tableau qui contient 3 fois les éléments de `T1`.
	Attention, la multiplication préserve l'ordre : `[1,2]*3 = [1,2,1,2,1,2]`.
	On verra que la multiplication ne pose pas de problèmes quand le tableau ne contient que **des types non mutables**.
	
On pourra ainsi facilement *pré-créer* un tableau rempli de 0 (ou autre valeur) et de taille $n$ avec la syntaxe : `[0] * n`.

#### Modifier un tableau

Pour modifier un élément particulier d'un tableau, on utilisera la même syntaxe qu'en algorithmie : `T[i] = nouvelle_valeur`.
On dit que: `on modifie l'élément de T à l'indice i`, c'est une affectation indicée.

Bien que l'on puisse modifier la taille d'une liste, on ne le fera pas en NSI et on ne verra donc pas comment le faire.

#### Parcourir un tableau

Pour parcourir un tableau, on utilisera la boucle for que l'on connaît déjà.
On aura les mêmes deux types de boucles qu'en algorithmie: indexée ou non.

##### Boucle indexée
Pour la boucle indexée, on réutilise les `range` que l'on connaît déjà, on va juste générer les indices *accessibles* du tableau.
Pour cela, il nous faut connaitre la taille du tableau, en python c'est la fonction `len` qui nous la donne.
On écrit donc `len(T)` pour avoir la taille (le nombre d'éléments) d'un tableau `T`.

Ensuite, on sait que les indices sont compris entre 0 et `len(T)-1`, car on a `len(T)` éléments.
La boucle for qui permet de parcourir tout les indices est donc:

```python
for indice in range(0,len(T)):
	...
```

À l'intérieur de cette boucle, on accède à *l'élément actuel* en indiçant T: `T[indice]`.
On a donc deux variables importantes lorsqu'on parcourt un tableau via un index:

* L'index lui même, qui nous donne la position actuelle dans le tableau
* L'élément situé à cet indice, qui est à la valeur dans le tableau.

Ceci nous permettra donc facilement de trouver la position d'un élément particulier.

##### Boucle implicite

On utilise une syntaxe proche de l'algorithmique pour cette boucle.
Si on a un tableau $T$, dont on ne se soucie pas de la taille, on le parcourt avec la boucle:

```python
for element in T:
	...
```

Avec cette boucle, on accède directement aux éléments et non aux indices, on dit qu'on **itère** sur les éléments du tableau.
Ici on a une seule variable importante : l'élément courant.

Cette boucle est bien adaptée pour récupérer tout les éléments d'une sous séquence, ou bien pour compter les éléments respectant une condition.

!!! tldr "for .. in ..."
	On peut remarquer que on utilise la même syntaxe pour parcourir un tableau ou un intervalle de valeurs.
	On pourrait donc croire que un intervalle n'est qu'un tableau de valeurs et qu'on pourrait donc écrire `range(10)[0]` pour le premier élément
	En vérité, les intervalles et les tableaux ont ce qu'on appelle *une interface commune*, c'est à dire un ensemble de *méthodes* (ou fonctions) qui permettent d'accéder à leurs éléments dans l'ordre.
	Cette propriété n'est pas à connaitre au lycée, mais il est intéressant de savoir que cela permet de réutiliser la même syntaxe pour des objets totalement différents.
	Vous pourrez même, bien plus tard, créer vos propres objets qui seront **itérables**.
	

