Il vous est conseillé d'avoir déjà lu la première partie du cours sur les tableaux dans [Représentation des types construits](../../repr-cons/tab/).

Nous allons ici parler des algorithmes courants sur les tableaux.

# Parcours d'un tableau

Pour calculer une valeur relative à un tableau (moyenne, sous tableau de valeurs respectant une propriété ...) on devra le **parcourir**.
Pour cela, nous écrirons **des boucles** de l'une des deux façons suivantes:

## Boucle indexée

On peut parcourir à l'aide d'un indice, qui va évoluer à chaque tour de boucle pour nous permettre d'accéder à chaque élément.
Si on a un tableau $T$ de taille $n$, on pourra par exemple faire:

```
indice_maximum = 0
Pour indice allant de 0 à n (exclus) faire
	si T[indice] > T[indice_maximum] alors
		maximum = indice
	finsi
finpour
afficher("Le maximum de T est situé à l'indice :", indice_maximum)
```

!!! note "Pour/finpour, si/finsi"
	Lorsqu'on écrit un algorithme, on est libre du langage utilisé, tant que celui-ci est **clair**.
	En python, l'indentation permet de délimiter des **blocs d'exécution** distincts.
	Pour cette année, en algorithmie, nous délimiterons explicitement le début et la fin des blocs.
	Pour cela, nous utiliserons la syntaxe suivante:
	```
	Instruction de début de bloc ... faire
		instructions à faire dans le bloc
		...
	Instruction de fin de bloc
	```	
	Ainsi, on pourra être sûr de où commence et où finit chaque bloc (notamment les répétitions).

## Les boucles implicites

Une autre méthode pour parcourir un tableau lorsqu'on ne se soucie pas de l'indice des éléments est possible.
Supposons que l'on ai un tableau $T$, dont on ne connaît pas la taille, on pourra écrire:

```
maximum = T[0]
Pour chaque élément e de T faire
	si e > maximum alors
		maximum = e
	finsi
finpour
afficher("Le maximum de T est ", maximum)
```

Avec cette méthode, on a accès aux éléments du tableau dans l'ordre, plus facilement (pas d'indexation à faire).
C'est aussi plus naturel à écrire, car on ne se soucie pas de l'organisation du tableau, on l'a **abstrait**.

Avec cette méthode, on ne peut pas modifier les éléments de $T$, car on a juste accès à l'élément, mais pas au tableau.
On peut imaginer que l'on a une copie de l'élément, locale à la boucle et qu'on ne travaille pas directement sur l'élément pour commencer.

## Quelle boucle utiliser ?

On peut faire un rapide comparatif des deux types de boucles pour parcourir un tableau:

| Boucle indexée                                       | Boucle implicite                                                |
| --                                                   | --                                                              |
| Accès à l'indice / Obligé d'indexer                  | Accès à l'élément / Obligé de calculer la position manuellement |
| Modification du tableau immédiate                    | Modification impossible en cours de boucle                      |
| Plus proche de *ce qui se passe réellement*          | Plus facile à conceptualiser pour nous                          |
| Demande de connaître / calculer la taille du tableau | Ne demande aucune connaissance sur le tableau                   |
# Calcul de valeurs relatives à un tableau
Ces algorithmes se basent sur le parcours du tableau vu précédemment.
## Calcul du nombre d'occurrences

* Entrée: Un tableau $T$ de longueur $n$ et un élément $e$.
* Sortie: Le nombre de fois où $e$ apparaît dans $T$

```
nombre_occurences <- 0
Pour indice allant de 0 à n faire
	si T[indice] vaut e alors
		nombre_occurences <- nombre_occurences + 1
	finsi
finpour
renvoyer nombre_occurences

## Comparaison de tableau

## Recherche d'un élément dans un tableau
