
!!! abstract "Le cours"
	La présentation sur le modèle Client Serveur: <https://codimd.apps.education.fr/p/1r7EcaIOi#/>

# TP Code côté serveur

Pour l'instant, on a vu uniquement des sites web **statiques** : le serveur envoie le
même site avec les mêmes fichiers (html, CSS, images, voire Javascript) à tout le monde.
On a utilisé la commande `python3 -m http.server PORT` qui crée un petit serveur web de site
statique.

Si on veut faire un site où les personnes qui l'utilisent reçoivent des pages personnalisées :

* si elles ont un compte avec des informations personnelles;
* si elles doivent pouvoir modifier le site internet et que d'autres le voient;

Alors ça ne suffit pas.

On va devoir créer comme serveur un vrai
programme qui va traiter les requêtes et renvoyer des pages HTML fabriquées automatiquement
en fonction de ces requêtes.

Beaucoup de langages de programmation peuvent être utilisé côté serveur. Historiquement c'était
souvent PHP, certains utilisent Javascript (langage qui avait été prévu pour être utilisé côté client
au départ), on va utiliser python bien sûr :wink:, avec la bibliothèque Flask.

!!! abstract "Pour commencer"
	Télécharger le fichier [tp_flask.zip](https://pnsi.xyz/ressources/tp_flask.zip) et extrayez tout le contenu
	quelque part : **dans l'explorateur de fichier, clic-droit et extraire ici sur le fichier ZIP téléchargé**

!!! bug "Pour installer Flask"
	Si vous avez l'erreur que Flask est introuvable, dans Thonny aller dans `Outils` > `Gérer les paquets`,
	cherchez "Flask" et installez le.

## Premier serveur

Ouvrez dans Thonny le fichier server1.py, dont voici le contenu:

```python
from flask import Flask, render_template, request
app = Flask(__name__)

@app.route("/")
def fonction_principale():
    ip = request.remote_addr
    return render_template('index1.html',ip_client=ip)

if __name__ == "__main__":
    app.run('0.0.0.0','8080')
```

Il y a des fonctionnalités spécifiques dans ce code qu'on ne connaît pas, voici une explication :

* Les deux premières lignes permettent de créer une application Flask
* Les deux dernières permettent de la lancer
  en écoutant sur `0.0.0.0` (c'est à dire sur toutes les interfaces réseau de l'ordinateur) sur le port TCP n°8080.
  Quand le programme est exécuté on peut donc accéder au site web depuis le réseau du lycée en tapant
  `http://ADRESSE:8080` dans la barre d'adresse où ADRESSE est l'adresse IP de votre machine (affichée sur le fond d'écran).
  Depuis votre propre machine vous pouvez aussi utiliser le raccourci `http://localhost:8080`.
* `@app.route("/")` est un décorateur (fonctionnalité avancée de python) qui permet que la fonction `fonction_principale` juste en dessous
  soit exécutée à chaque fois qu'une requête est faite sur le chemin `/`, c'est à dire la page principale du site.
* Cette fonction récupère juste l'adresse IP qui a envoyé la requête (fournie par Flask dans `request.remote_addr`) et renvoie
  un appel à la fonction `render_template` qui va renvoyer la page html fournie en paramètre après l'avoir modifiée.

Vous pouvez lire cette page HTML `index1.html` qui est dans le sous-dossier `templates`:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Site 1</title>
  </head>
  <body>
    <h1>Premier site</h1>
    <p>Bonjour, merci de me visiter depuis l'adresse ip {{ip_client}}</p>
  </body>
</html>
```

C'est une page normale, sauf pour la partie entre double accolades `{{ip_client}}`. Et comme on a mis `ip_client=ip` dans les
paramètres de `render_template` dans le code python, cette page va être modifiée
pour remplacer `{{ip_client}}` par la valeur de la variable `ip`.

!!! example "Test"
	Exécutez le code python dans Thonny, et aller sur la page [http://localhost:8080](http://localhost:8080).
	Qu'est-ce qui est affiché à la place de **ip_client** ? Vérifier dans le code HTML avec Ctrl+u.
	Vous pouvez demander à la personne à côté de vous d'aller sur http://VOTRE_IP:8080 et voir ce qu'elle obtient.

!!! example "Quelques questions"
	* Lancez dans un nouveau terminal la commande `curl -v http://localhost:8080` et retrouvez la requête **et** la réponse.
		* Quel est le code de retour de la réponse ? Que signifie-t-il ?
		* Quel est le type de la requête ?
		* Quel est **l'hôte** visé par la requête ?
		* Quel est la taille de la réponse ?
	* Même questions avec l'URL `http://localhost:8080/page_secrete.html`
	* Même questions avec l'URL `http://duck.co` (on pourra utiliser la commande `curl -v URL 2>&1  | grep "^[>,<] "` pour récupérer uniquement les entêtes.
		* Ouvrez https://duck.co avec un navigateur, quelle page est affichée ? Faites le lien avec la réponse reçue par curl.

	Pour aller plus loin:

	* Essayez une de ces page avec une requête de type **HEAD** quelle sont les différences ? (voir `man curl` et chercher `--head` pour voir comment lancer une requête HEAD).
	* Renseignez vous sur les autres types de requête HTTP.

## Deuxième serveur : formulaires et méthodes POST et GET

Ouvrir dans Thonny le fichier serveur2.py.

!!! info "Quelques explications"
	Il y a cette fois deux fonction :
	
	* La première affiche pour le chemin "/" la page avec le formulaire, c'est à dire le fichier index2.html
	* La deuxième affiche la page pour le chemin "/resultat". Comment va-t-on arriver là ? La réponse est
	  dans l'attribut _action_ de la balise form dans index2.html :wink:
	* Cette fonction récupère les données envoyées avec la méthode GET par le formulaire, et les affiche en
	  modifiant le fichier resultat.html
	  
* Lancez le serveur
* Ouvrez la page dans le navigateur
* Regardez le code source
* Complétez et envoyer le formulaire
* Regardez le résultat, ainsi que le code source de la page de résultat

!!! example "Exercice"
	On veut demander en plus un message dans le formulaire, et afficher
	ensuite la version chiffrée du message avec la méthode ROT13 (qui décale
	toutes les lettres de 13 places). À vous de jouer, il faudra faire trois
	étapes :
	
	* Ajouter dans le fichier html index2.html une demande de message et
	  une balise input pour pouvoir l'écrire. Vous pouvez l'ouvrir avec Geany, il est dans le sous-dossier
	  templates. Choisissez type="text" et
	  n'oubliez pas de choisir un attribut name pour pouvoir récupérer la valeur après.
	* Dans le code de serveur2.py, récupérer la valeur du message dans une
	  variable (comme pour le prénom). Vous pouvez copier/coller cette fonction
	  et l'utiliser pour calculer le message chiffré. Ajoutez ensuite un argument
	  dans l'appel à render_template pour pouvoir utiliser le message chiffré dans le résultat.
	  
	  ```python
	  def rot13(s):
		  s = s.upper()
		  res = ""
		  for i in range(len(s)):
			  if 'A' <= s[i] <= 'Z':
				  res += chr(((ord(s[i])-ord('A')+13)%26)+ord('A'))
			  else:
				  res += s[i]
	      return res
      ```
	  
	* Dans le fichier html resultat.html, ajouter du texte et un élément entre
	  double accolades pour afficher le message chiffré.
	  
Que se passe-t-il si vous remettez le message chiffré dans le formulaire de départ ?

## Troisième serveur : un site de messagerie instantanée !

On aurait pu obtenir les fonctionnalités précédentes avec un site statique, en
utilisant du code Javascript qui ferait les calculs dans le navigateur du client,
sans rien renvoyer au serveur.

Mais notre serveur en python est capable de faire que les personnes qui visitent le
site modifient des données et interagissent entre elles.

Ouvrez dans Thonny le fichier serveur3.py et exécutez le pour avoir un exemple.

Quelle méthode est utilisée cette fois pour le formulaire ? Est-ce que les données sont visibles dans l'URL ?

Vous pouvez demander à la personne à côté de vous de s'y connecter pour vérifier que tout le monde voit les messages.

!!! example "Exercices si vous avez le temps"
	* \* Ajoutez un champ pseudo qui s'affichera devant le message pour savoir qui l'a posté
	(le plus simple est de l'accoler directement devant le message avant de le mettre dans liste_messages).
	* \*\* Ajouter entre parenthèses l'adresse IP de la personne qui a posté chaque message après son pseudo.
	* \*\*\* Gardez seulement les 10 derniers messages et affichez les à l'envers, le plus récent en haut.
	* Mettez un peu de CSS pour rendre ça plus joli ?
	
