# Interaction sur le web

**Objectif :** Devine la couleur
Ce TP/Projet est **basé** sur le travail de Susam Pal, https://susam.net/myrgb.html.

## Préambule

Les deux sections suivantes sont relativement simples.
Ce ne sont que des rappels sur ce que vous connaissez déjà en HTML et CSS.
Elles sont cependant **très** nécessaires, suivez les donc avec attention.

Pour rappel:

* [Le cours HTML et CSS](https://fabricenativel.github.io/Premiere/pdf/C8/C8-cours.pdf#page=30)
* [L'activité sur le CSS](https://fabricenativel.github.io/Premiere/pdf/C8/Act8-2.pdf)
* [La documentation des balises](https://developer.mozilla.org/fr/docs/Web/HTML/Element)
* [La documentation des propriétés CSS](https://developer.mozilla.org/fr/docs/Web/HTML/Element)

Enfin, pour ce TP vous utiliserez uniquement Microsoft Edge au lycée.

!!! info "Pourquoi Edge et pas Firefox ?"
    Au lycée, Firefox est *limité* pour vous empécher l'accès au code source et à la console Javascript.
    Edge n'est pas limité lui, nous l'utiliserons donc au lycée.
    Vous êtes cependant libre d'utiliser Firefox chez vous pour faire ce TP.

### Rappels HTML


Voici la structure que nous avons vu en cours, agrémentée de composants *sémantiques*.
```html title="Structure de base d'une page HTML"
<!doctype html>
<html lang=fr>
    <head>
        <meta charset="utf-8">
        <title>Ma super page</title>
    </head>
    <body>
        <header>
            <h1>
                Un titre
            </h1>
        </header>
        <main>
            ...
        </main>
        <footer>
            <p>
                Un site fait par ...
            </p>
        </footer>
    </body>
</html>
```
	
??? note "Copiez cette structure de base dans un fichier `rappels.html`."
	1. Ouvrez *Notepad++*;
	2. créez un fichier nommé `rappels.html`; **Attention au point et il n'y a pas d'espaces**
    3. copiez la structure de base;
	4. enregistrez le fichier.
	
??? tip "Ouvrez le fichier avec Microsoft Edge."
	1. Ouvrez *l'explorateur de fichiers*;
	2. naviguez jusqu'au dossier contenant `rappels.html`;
	3. avec un clic droit sur le fichier, allez sur "Ouvrir avec", puis "Microsoft Edge".

Répondez ensuite dans votre fichier HTML aux questions suivantes, en utilisant des **commentaires HTML** et en codant les fonctionnalités le cas échéant.

* Expliquez le rôle des trois balises `header,main` et `footer`.
* Modifiez le titre de l'onglet en `Rappels` et de la page en `Rappels sur le HTML et le CSS`.
* Dans le `footer` ajoutez votre nom prénom ainsi que le badge d'une licence au choix ([Les badges](https://www.unesco.org/fr/open-access/creative-commons))
    * *Si vous le souhaitez, vous pouvez ajouter le lien vers l'explication de la licence (dit Deed) en français ([Les licences](https://creativecommons.org/licenses/list.fr#international-40)))*
	
??? danger "10 minutes se sont écoulées ou j'ai fini"
	```html title="La structure de base commentée"
	<!doctype html>
    <html lang=fr>
        <head>
            <meta charset="utf-8">
            <title>Rappels</title>
        </head>
        <body>
            <header>
				<!-- La balise header contient des éléments "introductifs"
					Il s'agit le plus souvent des titres, logos et éléments de navigation.
					C'est l'entête affiché.
				-->
                <h1>
                    Rappels sur le HTML et le CSS
                </h1>
            </header>
            <main>
				<!-- La balise main contient la majorité des éléments.
					C'est ici que l'on va trouver le sujet ou la fonctionnalitéprincipale.
					On a une seule balise main par document.
				-->
                ...
            </main>
            <footer>
				<!-- La balise footer contient les informations relatives à l'autrice
					au droit d'auteur ou aux liens vers d'autres documents.
					C'est le pied de page.
				-->
                <p>
                    Un site fait par Marius Monnier, sous licence
					<a href="https://creativecommons.org/licenses/by-sa/3.0/fr/deed.fr">
						CC-BY-SA 3.0
						<img src="https://www.unesco.org/sites/default/files/styles/paragraph_medium_desktop/public/2024-01/by-sa-final_0.jpg.webp?itok=lVCAje7D" alt="logo de la licence CC-BY-SA 3.0">
					</a>
                </p>
            </footer>
        </body>
    </html>
	```

### Rappels CSS

Vous allez maintenant styliser un peu cette page.

??? note "Créez un fichier `rappels.css` vide à coté de `rappels.html`."
	Dans *Notepad++*:
	
	1. créez un nouveau fichier;
	2. cliquez sur son nom (*New file*) puis sur *Rename* et tapez `rappels.css`.
	4. enregistrez le fichier. ++ctrl+s++
	
Ajoutez dedans la règle suivante:

```css title="Une régle mystérieuse"
/* Ceci est un commentaire CSS */
h1 {
	text-decoration: underline;
}
```

??? tip "**Ajouter un lien vers ce fichier dans l'entête de `rappels.html`**."
	Dans *Notepad++*:
	
	1. Sélectionnez le fichier `rappels.html`;
	2. entre les deux balises `<head>` et `</head>`, créez une nouvelle ligne vide;
	3. écrivez y : `<link rel="stylesheet" href="rappels.css">`.
	
Répondez ensuite dans votre fichier CSS aux questions suivantes, en utilisant des **commentaires CSS** et en codant les fonctionnalités le cas échéant.

* Expliquez quel élément cible la règle proposée (ligne 2)
* Expliquez la règle appliquée (ligne 3)
* Stylisez les titres en: **centrés**, en taille **2em** et d'une couleur de votre choix.
* Stylisez le fond de **toute la page** dans une couleur de votre choix au format `#rgb`.


??? tip "Je ne comprends rien à ce qui est écrit au dessus"
	* [rappel sur la notation hexadécimale](https://developer.mozilla.org/fr/docs/web/css/css_colors/applying_color#la_notation_hexad%C3%A9cimale)
	* [les sélecteurs](https://developer.mozilla.org/fr/docs/Learn/Getting_started_with_the_web/CSS_basics#les_diff%C3%A9rents_types_de_s%C3%A9lecteurs)
	* [la propriété de fond](https://developer.mozilla.org/fr/docs/Web/CSS/background-color)
	
??? danger "5 minutes se sont écoulées ou j'ai fini"
	```css title="le code attendu et commenté"
	h1 { /* On cible tout les titres de niveau 1 */
		text-decoration: underline; /*Ils seront soulignés */
		text-align: center; /*centrage du texte*/
		font-size: 2em; /* taille de la police*/
		color: aliceblue; /* une couleur au choix */
	}
	body { /*La page entière*/
		background-color: #F06; /*La couleur de fond est F06 soit FF0066 soit R=255,G=0,B=102*/
	}
	```


## Interaction

??? tip "Monsieur j'ai perdu mes fichiers"
    Voici les codes attendus à la fin de la partie précédente:
    ```html title="rappels.html"
    <!doctype html>
    <html lang=fr>
        <head>
            <meta charset="utf-8">
            <title>Rappels</title>
            <link rel="stylesheet" href="rappels.css">
        </head>
        <body>
            <header>
                <h1>
                    Rappels sur le HTML et le CSS
                </h1>
            </header>
            <main>
                ...
            </main>
            <footer>
                <p>
                    Un site fait par Marius Monnier, sous licence
					<a href="https://creativecommons.org/licenses/by-sa/3.0/fr/deed.fr">
						CC-BY-SA 3.0
						<img src="https://www.unesco.org/sites/default/files/styles/paragraph_medium_desktop/public/2024-01/by-sa-final_0.jpg.webp?itok=lVCAje7D" alt="logo de la licence CC-BY-SA 3.0">
					</a>
                </p>
            </footer>
        </body>
    </html>
    ```
    ```css title="rappels.css"
	h1 { 
		text-decoration: underline; 
		text-align: center; 
		font-size: 2em; 
		color: aliceblue; 
	}
	
	body { 
		background-color: #F06; 
	}
	```

La page que vous avez créé est **statique** (non interactive).
Vous allez maintenant faire en sorte qu'une personne puisse interagir avec grâce à des boutons et la rendre ainsi plus *vivante*.

### Interaction basique

Dans votre navigateur, ouvrez **la console javascript**:

* Pour Firefox: ++f12++ puis `Console` (Icone: <i class="fa fa-terminal" style="border: solid 2px; border-radius: 20%; padding: 0px 2px"></i>)
* Pour Edge: ++ctrl+shift+j++ puis `Console`.

#### Exécution de code basique

Dans le terminal javascript qui est désormais disponible essayez les commandes suivantes:

* `2+2` ++return++
* `10**2` ++return++
* `let a = 10` ++return++
* `a = a + 100` ++return++
* `if ( a == 120 ) { gagner100euros() }` ++return++

Répondez aux questions suivantes en commentaires HTML:

* Comment **définir** une variable en Javascript ?
* Comment **changer** la valeur d'une variable en Jascript ?
* Comment faire une condition en Javascript ?

??? success "5 minutes ou bien j'ai fini"
    Le Javascript permet d'utiliser des constructions très similaires à python:
    
    * *La définition* de variable (création) se fait avec `let <variable> = <valeur ou expression>`;
    * *l'affectation* (changement) se fait avec `<variable> = <valeur ou expression>`;
    * *les conditions* s'écrivent avec la structure suivante:
    ```javascript title="Les conditions en javascript"
    if (ma condition) {
        //exécuté uniquement si ma condition est vraie.
        instruction1;
        ...
    }
    else if (ma condition 2){
        //exécuté si ma condition est fausse ET ma condition 2 est vraie.
        ...
    }
    else {
        //exécuté si les deux conditions précédentes sont fausses.
        ...
    }
    ```
    On notera particulièrement que:
    * les **blocs d'instructions** sont délimités par des accolades `{...}`;
    * les conditions s'écrivent entre parenthèses `()`;
    * les instructions sont terminées par un point-virgule `;`

#### Parler avec le clavier

Tapez maintenant les instructions suivantes: 

* `alert('bonjour')` ++return++
* `prompt('bonjour')` ++return++
* `texte = prompt('Qui êtes vous ?') ; alert(texte)` ++return++

Répondez dans des commentaires à ces questions:

* Que fait le navigateur lors d'une instruction `alert` ?
* Comment modifier l'instruction pour afficher **au revoir** ?
* Que fait l'instruction `prompt` ?

??? success "5 minutes ou bien j'ai fini"
    Javascript permet d'interagir avec le navigateur.
    L'interaction la plus simple est l'affichage de texte, on utilise pour cela la fonction `alert(<votre texte>)`.
    On pourra aussi récupérer du texte avec l'instruction `prompt(<texte affiché>)` qui renvoie le texte tapé au clavier.
    Enfin, on peut exécuter plusieurs instructions avec le point-virgule: `inst1;inst2`.


#### Éléments interactifs

Ajoutez le code suivant dans le `main` de votre page `rappels.html`, puis actualisez votre page.
Cliquez sur le bouton qui est désormais là.

```html title="Un bouton mystérieux"
<button onclick="alert(atob('Q2Ugbidlc3QgcGFzIHRy6HMgc+ljdXJpc+k='))"> Cliquez ici </button>
```

L'attribut `onclick` permet de définir un code javascript à exécuter lors d'un certain évènement.

* Quel évènement déclenche l'exécution du bouton ?
* Que se passe-t-il lorsque cet évènement survient sur le bouton ?
* *Bonus* Cherchez comment faire pour afficher une alerte **au survol** du bouton (**quand la souris passe au dessus**).

!!! success "À retenir"
    Un bouton est un moyen simple d'interagir avec l'utilisateur.
    
    L'attribut `onclick` permet de déclencher une action **lors du clic** sur l'élément le portant.
    L'action est un code javascript, par exemple `alert('blabla')` ouvrira une fenêtre pop-up contenant le texte 'blabla'.
    
    D'autres attributs existent, comme `onmouseover,onload,...`


??? question "Bonne ou mauvaise pratique"

	À propos des attributs *on..*, le site MDN dit:
		
	> Vous trouverez des équivalents d'attributs HTML pour la plupart des propriétés du gestionnaire d'événement; cependant, vous ne devriez pas les utiliser — ils sont considérés comme une mauvaise pratique. Il peut sembler facile d'utiliser un attribut de gestionnaire d'événement si vous voulez avancer rapidement, mais ils deviennent rapidement ingérables et inefficaces.
	> Pour commencer, ce n'est pas une bonne idée de mélanger votre HTML et votre JavaScript, car il deviennent difficile à analyser — garder votre JavaScript au même endroit est préférable; s'il se trouve dans un fichier séparé, vous pourrez l'appliquer à plusieurs documents HTML.
	> Même dans un fichier unique, les gestionnaires d'événement en ligne ne sont pas une bonne idée. Un bouton ça va, mais que faire si vous avez 100 boutons ? Vous devez ajouter 100 attributs au fichier; la maintenance se transformerait très vite en un cauchemar.
	> <https://developer.mozilla.org/fr/docs/Learn/JavaScript/Building_blocks/Events#les_gestionnaires_d%C3%A9v%C3%A9nements_en_ligne_ne_les_utilisez_pas_!>

	Nous les utilisons ici dans un cadre pédagogique et pour vous simplifier la tache, cependant dans le monde professionnel il faudra plutôt recourir aux **gestionnaires d'évènements Javascript**.

### Interagir avec le contenu.

!!! danger "Attention, changement de fichier."
    À partir d'ici, vous travaillerez dans un nouveau fichier html de votre choix.
    Vous avez bien sûr toujours accés à ce que vous avez fait dans `rappels.html` et `rappels.css`.


Commençons par définir une structure de base que nous allons rendre interactive petit à petit.

* Reprenez la structure de base;
* ajoutez dans la section `main` un paragraphe vide;
* ajoutez au paragraphe un attribut `id` ayant pour valeur `result`.

??? danger "Je n'ai pas compris"
    ```html title="inter.html"
    <!doctype html>
    <html lang=fr>
        <head>
            <meta charset="utf-8">
            <title>Ma super page</title>
        </head>
        <body>
            <header>
                <h1>
                    Un titre
                </h1>
            </header>
            <main>
                <p id="result"></p>
            </main>
            <footer>
                <p>
                    Un site fait par ...
                </p>
            </footer>
        </body>
    </html>
    ```

Nous allons créer un bouton permettant de modifier le paragraphe petit à petit.
Pour cela, il nous faut séparer **le code javascript** et le code **HTML**, car notre action ne sera pas aussi simple qu'un `alert`.

* Créez un fichier `interaction.js` dans le même repértoire que votre fichier HTML.
* Copiez collez le code suivant dans `interaction.js`.

```javascript title="Des fonctions mystérieuses"
//Ceci est un commentaire Javascript
let parResult = document.getElementById('result');

function ditBonjour() {
    alert('bonjour');
}

function changePar() {
    parResult.innerHTML = "Regardez le code source !";
}
```

* Dans votre page HTML ajoutez **au dessus du `</body>`** la ligne suivante:

```html
<script src="interaction.js"></script>
```

* Ajoutez un bouton dans votre page HTML, avec l'attribut onclick ayant pour valeur `ditBonjour()`, **attention aux parenthèses et à la majuscule**.

??? danger "Résultat attendu"
    Dans le html:
    ```html
    <!doctype html>
    <html lang=fr>
        <head>...</head>
        <body>
            <header>...</header>
            <main>
                <p id="result"></p>
                <button onclick="ditBonjour()">Cliquez moi</button>
            </main>
            <footer>...</footer>
            <script src="interaction.js"></script>
        </body>
    </html>
    ```

    Dans le JS:
    ```javascript
    //Ceci est un commentaire Javascript
    let parResult = document.getElementById('result');

    function ditBonjour() {
        alert('bonjour')
    }

    function changePar() {
        parResult.innerHTML = "Regardez le code source !"
    }
    ```

Rechargez la page HTML et cliquez sur votre bouton.

* Quelle interaction a eu lieu encore une fois ?

Dans le bouton HTML, modifiez la valeur de `onclick` en `changePar()` au lieu de `ditBonjour()` et rechargez votre page HTML.

* Quelle interaction a eu lieu cette fois ?
* Comment définir une fonction sans argument en Javascript ?
* Quelle ligne permet de trouver le paragraphe dans la page ?
* Quelle ligne permet de changer l'intérieur du paragraphe ?

??? success "Correction"
    Ici l'attribut `onclick` n'a pas changé, l'interaction est donc déclenchée au clic.
    Le message est par contre affiché **dans la page web** au lieu d'une fenêtre pop-up.

    Pour accéder au paragraphe on utilise: `let parResult = document.getElementById("result")`.
    Comme son nom l'indique, la fonction permet de récupérer un **élement** de la page connaissant son **id**.

    Pour changer l'intérieur du paragraphe on utilise `parResult.innerHTML = <valeur>`.



Vous allez maintenant essayer vous même de modifier le contenu d'un élément de la page.
Pour cela, dans votre fichier HTML: 

* ajoutez un titre avec un `id` que vous choisirez
* ajoutez un bouton pour le modifier (avec son attribut `onclick`).

??? success "Code attendu"
    ```html title="Un titre et un bouton" hl_lines="4 5"
    <body>
            <header>
                ...
                <h1 id="monTitre"> Un nouveau titre</h1>
                <button onclick="changeTitre()">
            </header>
            <main>
                <p id="result"></p>
                <button onclick="changePar()">
            </main>
            ...
            <script src="interaction.js">
    </body>
    ```

Dans le fichier `interaction.js`: 

* ajoutez une variable *similaire* à `parResult` pour trouver votre titre
* ajoutez une fonction *similaire* à `changePar()` pour modifier votre titre.
* appelez cette fonction dans l'attribut onclick de votre bouton.

??? success "Code attendu"
    ```javascript title="Fichier JS final" hl_lines="10-13"
    let parResult = document.getElementById('result');

    function ditBonjour() {
        alert('bonjour');
    }

    function changePar() {
        parResult.innerHTML = "Regardez le code source !";
    }
    let titre = document.getElementById('monTitre');
    function changeTitre() {
       titre.innerHTML = "Nouveau titre";
    }
    ```

#### Un peu d'aléatoire

Nous aimerions pouvoir faire un peu mieux qu'afficher un texte fixe, nous allons donc ajouter un peu d'aléatoire.
En javascript, nous avons accès à une fonction pour obtenir des nombres aléatoires:

> La fonction Math.random() renvoie un nombre à virgule aléatoire compris dans l'intervalle $[0, 1[$ (ce qui signifie que 0 est compris dans l'intervalle mais que 1 en est exclu)
> [Pour en savoir plus](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Math/random)

Ajoutez le code suivant à la fin de `interaction.js`:

```javascript
function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function changeParAlea(){
    parResult.innerHTML = "J'ai " + getRandomInt(10) + " pommes";
}
```

Modifiez l'attribut onclick du bouton pour changer le paragraphe en: `changeParAlea()`

Rechargez la page puis cliquez plusieurs fois sur le bouton pour observer le changement de comportement.

* Comment définit-on une fonction **en général** en javascript ? (avec ou sans arguments).
* Modifiez `changeParAlea` pour afficher deux nombres aléatoires différents.

!!! success "Un peu de cours"
    En javascript, une fonction est définie comme:
    ```javascript
    function nomDeLaFonction(argument1,argument2,...) {
        instruction1;
        instruction2;
        ...
        return uneValeur:
    }
    ```

    La fonction `Math.floor` permet d'arrondir, donc de récupérer un nombre entier depuis un nombre à virgule.
    Pour générer un nombre aléatoire, on pourra utiliser `getRandomInt(...)` avec une valeur maximale comme argument, à condition d'avoir définit cette fonction avant.

     on fera:

    ```javascript title="Pour générer deux entiers aléatoires" hl_lines="6"
    function getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }

    function changeParAlea(){
        parResult.innerHTML = "J'ai " + getRandomInt(10) + " pommes et "+ getRandomInt(1000) + "poires";
    }
    ```

### Interaction avec le style

On a vu précédemment comment modifier le contenu de balises HTML.
Cependant, en Javascript on a aussi accès au style CSS des éléments.

Copiez les deux codes suivants:

```html title="Dans le main de votre fichier HTML"
<p id="stylise"> Ce texte n'a aucun style. </p>
<button onclick="modifieStyle()">Modification !</button>
```

```javascript title="A la fin de votre fichier Javascript"
let parStyle = document.getElementById('stylise');

function modifieStyle(){
    parStyle.style.fontSize = '10em';
    //parStyle.style.border = 'solid blue 2px';
    //parStyle.style.backgroundColor = 'red';
}
```

Enregistrez vos fichiers et rechargez votre page HTML pour tester l'effet du bouton.
Ensuite enlevez les commentaires (`//`) pour voir comment évolue le style.

* Quelle est la syntaxe pour accéder à la propriété CSS `prop` de l'élément dans la variable `elem` en Javascript ?
* Comment est délimitée une chaîne de caractères en Javascript ?
* Quel type de donnée doit-on affecter à ces propriétés CSS ?
* *Bonus* Les noms des propriétés CSS ne sont pas tout à fait les mêmes en Javascript, expliquez comment passer du CSS au JS (`border-size` vers `borderSize`).

Vous allez maintenant pouvoir faire votre propre modification stylistique, qui en plus sera aléatoire !
!!! tip "Pour ne pas bloquer"
    Écrivez un texte dans le paragraphe `result` pour que la suite soit plus simple, ou alors pensez bien à le remplir grâce à notre bouton javascript avant de modifier les styles.

Relisez bien comment obtenir un nombre aléatoire en Javascript et complétez les deux fonctions suivantes dans votre fichier interaction.js

```javascript title="A copier à la fin de interaction.js"
  function hex (r, g, b) {
      //Écriture de la couleur (r,g,b) en hexadécimal 3 lettres.
      let rs = r.toString(16)
      let gs = g.toString(16)
      let bs = b.toString(16)
      return ('#' + rs + gs + bs).toUpperCase()
  }

function changeTailleTexte(){
    //Cette fonction doit changer la TAILLE DE LA POLICE utilisée dans le paragraphe d'id result.
    //Cette taille sera un entier aléatoire, à vous de choisir le nombre maximum.
    ... = ...(...) + 'px'
}

function changeCouleurFond(){
    //Cette fonction change la COULEUR DE FOND utilisée dans le paragraphe d'id result.
    //Cette couleur est écrite comme #rgb grâce à la fonction hex qui traduit trois nombres entre 0 et 15 en format hexadécimal.
    ... = hex(...,...,...);
}
```

Ajoutez maintenant deux boutons qui appeleront ces fonctions et testez les.

??? success "Correction"
    ```javascript title="Fichier JS final"
        let parResult = document.getElementById('result');
        let parStyle = document.getElementById('stylise');

        function modifieStyle(){
            parStyle.style.fontSize = '10em';
            //parStyle.style.border = 'solid blue 2px';
            //parStyle.style.backgroundColor = 'red';
        }
        function hex (r, g, b) {
            //Écriture de la couleur (r,g,b) en hexadécimal 3 lettres.
            let rs = r.toString(16)
            let gs = g.toString(16)
            let bs = b.toString(16)
            return ('#' + rs + gs + bs).toUpperCase()
        }

        function getRandomInt(max) {
            return Math.floor(Math.random() * max);
        }
        function changeParAlea(){
            parResult.innerHTML = "J'ai " + getRandomInt(10) + " pommes et "+ getRandomInt(1000) + "poires";
        }

        function changeTailleTexte(){
            //Cette fonction doit changer la TAILLE DE LA POLICE utilisée dans le paragraphe d'id result.
            //Cette taille sera un entier aléatoire, à vous de choisir le nombre maximum.
            parStyle.style.fontSize = getRandomInt(50) + 'px'
        }

        function changeCouleurFond(){
            //Cette fonction change la COULEUR DE FOND utilisée dans le paragraphe d'id result.
            //Cette couleur est écrite comme #rgb grâce à la fonction hex qui traduit trois nombres entre 0 et 15 en format hexadécimal.
            parStyle.style.backgroundColor = hex(getRandomInt(15),getRandomInt(15),getRandomInt(15));
        }
        let titre = document.getElementById('monTitre');
        function changeTitre() {
        titre.innerHTML = "Nouveau titre";
        }
    ```

    ```html title="Fichier HTML final"
    <!doctype html>
    <html>
        <head>
            <meta charset="utf-8"></meta>
            <title>Page avec de l'interaction</title>
        </head>
        <body>
            <header>
                <h1 id="monTitre"> Un nouveau titre</h1>
                <button onclick="changeTitre()">
            </header>
            <main>
                <p id="result"></p>
                <button onclick="changePar()">

                <p id="stylise">Je suis un texte sans style.</p>
                <button onclick="changeTailleTexte()">Taille</button>
                <button onclick="changeCouleurFond()">Couleur</button>
                <button onclick="modifieStyle()">Tout d'un coup</button>
            </main>
            <script src="interaction.js"></script>
        </body>
    </html>
    ```

### Interaction avancée


!!! danger "Suppression des anciennes variables Javascript"
    A partir d'ici nous n'avons plus besoin des anciens codes JS, supprimez les donc.
    Idem pour le HTML, nous repartons avec le fichier de base:

    ```html title="Fichier HTML de base"
    <!doctype html>
    <html>
        <head>
            <meta charset="utf-8"></meta>
            <title>Page avec de l'interaction</title>
        </head>
        <body>
            <header>
            </header>
            <main>
            </main>
            <script src="interaction.js"></script>
        </body>
    </html>
    ```

Les boutons sont pratiques pour des interactions simples, mais il y a d'autres éléments HTML qui nous permettrons d'obtenir des interactions plus avancées.

Nous allons ici utiliser les sliders, qui permettent de choisir une valeur sur une échelle.

Voici les codes HTML,JS et CSS qui vous permettront d'avoir un slider sur 5 valeurs avec un affichage dynamique de la valeur.

```html title="A ajouter dans le HTML, dans main"
<ol class="ruler">
    <li>1</li>
    <li>2</li>
    <li>3</li>
    <li>4</li>
    <li>5</li>
</ol>
<div class="slider">
  <input type="range" min="1" max="5" step="1" value="2" id="monSlider"
         onchange="lireValeur()" oninput="lireValeur()">
</div>
<p id="texteSlider">Un super texte</p>
```

```css title="A ajouter dans le fichier CSS"
ol.ruler, div.slider {
  display: flex;
  margin: 1em auto;
}
ol.ruler li {
  display: flex;
  width: 100%;
}
div.slider label {
  padding-right: 1em;
}
div.slider input {
  width: 100%;
}
```

```javascript title="A écrire dans le fichier Javascript"
function hex (r, g, b) {
    //Écriture de la couleur (r,g,b) en hexadécimal 3 lettres.
    let rs = r.toString(16)
    let gs = g.toString(16)
    let bs = b.toString(16)
    return ('#' + rs + gs + bs).toUpperCase()
}
let slider = document.getElementById('monSlider');
let parResSlider = document.getElementById('texteSlider');
function lireValeur(){
    let valeurSlider = slider.valueAsNumber;
    parResSlider.innerHTML = 'La valeur du slider est désormais '+valeurSlider;
}
```

Une fois ces codes ajoutés à votre page, rechargez la et essayez de bouger le slider pour voir comment évolue le paragraphe.

* Dans quelle variable est stocké l'élément HTML du slider ? l'élément du paragraphe ?
* Quelle instruction javascript permet de récupérer la valeur du slider ?
* Le slider définit deux attributs pour les événements, lesquels et à quoi servent-ils ? (essayez d'en supprimer un)
* À quoi sert l'attribut `value` du slider ?

??? success "Correction"
    Le slider est stocké dans la variable `slider`, le paragraphe dans `parResSlider`.
    La ligne qui permet de récupérer la valeur est `slider.valueAsNumber`, on stocke ensuite dans la variable `valeurSlider`.
    Les deux attributs définis sont `onchange` et `oninput`, on lira la documentation de MDN pour mieux les comprendre.
    L'attribut `value` permet de positionner le slider au départ.

Vous avez désormais un input sous forme de slider, grâce à lui vous pouvez laisser l'utilisateur changer plus dynamiquement les valeurs qu'avec un bouton.
Le but est maintenant de changer en plus du texte du paragraphe, sa couleur de fond.
Pour cela vous allez devoir ajouter à la fonction `lireValeur` les lignes suivantes:

* créer une variable `couleur` égale à la couleur `#vvv` avec `v` la valeur de la variable `valeurSlider` (indice: Utilisez la fonction `hex` vue précédemment);
* affecter à la propriété `backgroundColor` de `parResSlider` la variable `couleur`.

??? success "Correction"
    ```javascript title="A ajouter à la fin de la fonction"
    let couleur = hex(valeurSlider,valeurSlider,valeurSlider);
    parResSlider.style.backgroundColor = couleur;
    ```

Une fois ceci fait, pensez à actualiser votre page pour tester votre travail.

* Combien de couleurs différentes peut-on avoir en fond ?
* Combien de valeurs différentes sont possibles pour un caractère hexadécimal ?

??? success "Correction"
    On peut ici avoir 5 nuances de gris différentes.
    Un caractère hexadécimal peut prendre 16 valeurs.

Modifiez le code HTML du slider pour avoir toutes les valeurs possibles en hexadécimal, pensez à mettre à jour la liste `ruler`.

??? success "Correction"
    ```html title="Modifications de \<ol\> et \<div\>" hl_lines="2 8-17 20"
    <ol class="ruler">
        <li>0</li>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
        <li>8</li>
        <li>9</li>
        <li>A</li>
        <li>B</li>
        <li>C</li>
        <li>D</li>
        <li>E</li>
        <li>F</li>
    </ol>
    <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="monSlider"
            onchange="lireValeur()" oninput="lireValeur()">
    </div>
    ```
* Que remarquez vous sur les couleurs que peut prendre le fond du paragraphe ?
* Comment pourrait-on obtenir plus de diversité dans les couleurs ?

??? success "Correction"
    Ici nous n'avons que 16 nuances de gris différentes au lieu de 256 couleurs.
    Pour obtenir plus de diversité il faudrait pouvoir modifier chaque composante de la couleur et donc avoir trois sliders.

#### De la diversité dans les couleurs

Vous avez pu observer que la couleur de fond est uniquement dans des tons gris.
Pour modifier cela, il nous faut pouvoir modifier indépendamment les canaux rouge, vert et bleu de la couleur de fond.
Nous allons donc modifier la page pour avoir trois sliders, un pour chaque canal.

* Modifiez le code HTML pour avoir trois sliders d'ids `rin`,`gin` et `bin`, qui peuvent prendre toutes les valeurs hexadécimales.
* Modifiez les variables javascript pour avoir accès à vos trois sliders via : `rin`,`bin` et `gin` (les variables peuvent avoir le même nom que les ids)/
* Modifiez la fonction `lireValeur` pour lire les valeurs des trois sliders et mettre à jour le fond du paragraphe en fonction. (la fonction `hex` est encore assez utile.)

??? success "Correction"
    ```html title="Le code html des trois sliders"
    ...
    <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="rin"
            onchange="lireValeur()" oninput="lireValeur()">
    </div>
    <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="gin"
            onchange="lireValeur()" oninput="lireValeur()">
    </div>
    <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="bin"
            onchange="lireValeur()" oninput="lireValeur()">
    </div>
    ```
    ```javascript title="Le code javascript total"
        function hex (r, g, b) {
            //Écriture de la couleur (r,g,b) en hexadécimal 3 lettres.
            let rs = r.toString(16)
            let gs = g.toString(16)
            let bs = b.toString(16)
            return ('#' + rs + gs + bs).toUpperCase()
        }

        let rin = document.getElementById('rin');
        let gin = document.getElementById('gin');
        let bin = document.getElementById('bin');
        let parResSlider = document.getElementById('texteSlider');

        function lireValeur(){
            let vr = rin.valueAsNumber;
            let vg = gin.valueAsNumber;
            let vb = bin.valueAsNumber;
            let couleur =  hex(vr,vg,vb);
            parResSlider.style.backgroundColor = couleur;
        }
    ```

#### Afficher le "format" de la couleur

En plus de voir la couleur, on aimerait pouvoir lire son code hexadécimal dans le paragraphe.

* Modifiez le texte affecté au paragraphe pour qu'il soit **Vous avez choisi #1B4** si les sliders sont positionnés sur 1,B et 4. (Vous a-t-on parlé de la fonction `hex` ?)

??? success "Correction"
    ```javascript title="A la fin de lireValeur"
    function lireValeur(){
        let vr = rin.valueAsNumber;
        let vg = gin.valueAsNumber;
        let vb = bin.valueAsNumber;
        let couleur =  hex(vr,vg,vb)
        parResSlider.style.backgroundColor = couleur;
        parResSlider.innerHTML = "Vous avez choisi" + couleur;
    }
    ```

#### N'afficher que sur demande

On aimerait désormais que la couleur ne soit pas mise à jour en continu mais seulement lors de l'appui sur un bouton.

Pour cela, nous allons séparer la fonction `lireValeur` en deux:

* `ecrireCouleur` qui mettra à jour le paragraphe `result` avec le texte `Vous avez choisi ...`.
* `afficherCouleur` qui mettra à jour la couleur de fond du paragraphe.

À vous de jouer:

* Créez les deux fonctions précédentes à partir de votre fonction `lireValeur` (qui ne doit plus exister à la fin).
* Modifiez les attributs des sliders pour qu'ils appellent `ecrireCouleur`
* Ajoutez un bouton `Afficher` qui change la couleur de fond du paragraphe.

??? success "Correction"
    ```javascript title="A la place de lireValeur"
    function ecrireCouleur(){
        let vr = rin.valueAsNumber;
        let vg = gin.valueAsNumber;
        let vb = bin.valueAsNumber;
        parResSlider.innerHTML = "Vous avez choisi : "+hex(vr,vg,vb);
    }

    function afficherCouleur(){
        let vr = rin.valueAsNumber;
        let vg = gin.valueAsNumber;
        let vb = bin.valueAsNumber;
        parResSlider.style.backgroundColor = hex(vr,vg,vb);
    }
    ```
    ```html title="Les sliders mis à jour"
    <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="rin"
            onchange="ecrireCouleur()" oninput="ecrireCouleur()">
    </div>
    <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="gin"
            onchange="ecrireCouleur()" oninput="ecrireCouleur()">
    </div>
    <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="bin"
            onchange="ecrireCouleur()" oninput="ecrireCouleur()">
    </div>
    <button onclick="afficherCouleur()"> Afficher </button>
    ```

## Le jeu

Maintenant que vous avez fait tout cela vous êtes prêt à faire le jeu complet.
L'idée du déroulé est la suivante:

* L'ordinateur choisit une couleur de fond au hasard pour la page entière.
* À l'origine les trois sliders sont positionnés sur 7
* L'utilisateur à un bouton 'Proposition #RGB' qui affiche dynamiquement **la valeur** des trois sliders (pas la couleur)
* Au clic sur ce bouton, on affiche soit "Gagné" soit "Proche à x%" avec x la distance à la couleur de fond.
* Le fond du paragraphe précédent (Gagné/Proche) doit être de la couleur sélectionnée par les sliders.

Pour vous aider à démarrer, un début de fichiers HTML, CSS et JS vous sont fournis ci-dessous:


```html title="index.html"
<!--
    Guess My RGB 0.1.0
    Source: https://github.com/susam/myrgb
    Copyright (c) 2024 Susam Pal

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    Modifié par Marius Monnier dans le cadre de l'enseignement de NSI.
    04/04/2024
    
  -->
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Devine mon RGB</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Un jeu de devinettes.">
    <link rel="stylesheet" href="style.css">
    <script src="interaction.js"></script>
  </head>
  <body onload="init()">
    <main>
      <h1>Devine la couleur</h1>
      <ol class="ruler"> <!-- La liste des valeurs possibles -->
        <li>0</li>
        <li>1</li>
        <li>...</li>
      </ol>
      <div class="slider">
        <input type="range" id="rin" name="rin" ...>
        <label for="rin">R</label>
      </div>
      <div class="slider">
        <input ...>
        <label for="gin">G</label>
      </div>
      <div ...>
        <input ...>
        <label for="bin">B</label>
      </div>
      <p id="info">Positionnez les sliders pour essayer de trouver la couleur de fond.</p>
      <button id="submit"  ...>Proposition : </button>
    </main>
    <div id="result"></div>
  </body>
</html>

```

```css title="style.css"
/* *** Partie fixe pour un affichage "satisfaisant" *** */
body {
    width: 100%;
    padding: 0;
    margin: 0;
}
ol.ruler {
    padding: 0;
    display: flex;
    width: calc(100% - 1em);
    justify-content: space-between;
}
div.slider {
    display: flex;
    width: 100%;
}
ol.ruler li {
    list-style-type: none;
}
div.slider input {
    width: 100%;
}

/* *** Partie Modifiable en partie *** */

main {
    background-color: ...; /* Un fond uni permettrait de mieux voir les sliders */
    padding: 1em;
    margin: 0;
}
h1 {
    /* à vous de styliser le titre de la page. */
}

button {
    /* Stylisez les boutons de la page comme vous le voulez */
}

#rin {
    accent-color: ...; /*Couleur de la barre du slider rouge */
}

#gin {
    accent-color: ...; /*Couleur de la barre du slider vert */
}

#bin {
    accent-color: ...; /*Couleur de la barre du slider bleu */
}
```


```javascript title="interaction.js"
'use strict'

//La couleur du fond décomposée en RGB.
let r,g,b;

// Nos trois sliders
let rin, gin, bin;

//Les éléments html que nous mettons à jour.
let info, submit, restart, result;

// Nombre d'essais.
//let ... = ... ;

function init () {
    /* Fonction appelée au chargement de la page pour créer toutes les variables HTML */
    
    //Liaison des variables JS avec les éléments HTML
    rin = document.getElementById('rin')
    gin = ...
    bin = ...
    
    //Donnée
    info = document.getElementById('info')
    submit = document.getElementById('submit')
    restart = document.getElementById('restart')
    result = document.getElementById('result')

    //Appel de la fonction qui met à jour le fond de la page et l'état des boutons
    setupGame()
}

function pickRGB () {
    //Choix d'une quantité aléatoire pour chaque composante entre 0 et F
    
    r = ...
    g = ...
    b = ...
}

//Initiatialisation des variables et de la couleur.
function setupGame () {

    //Appel de la fonction qui choisit une couleur aléatoire dans r,g et b
    ...()
    
    // Désactivation du bouton envoyer et redémarrer au début du jeu.
    submit.disabled = true
    restart.disabled = true

    // Aucun résultat et aucun essai.
    result.innerHTML = ''
    

    //La couleur de fond est obtenue grâce à hex, appelée avec r, g et b
    document.body.style.background = ...
}

// Lecture des sliders 
// mise à jour du bouton "Proposition : #RGB"
function lire () {
    ...
    ...
    ...

    ...
    
    //On peut désormais cliquer dessus.
    submit.disabled = false
}

// Fonction utilitaire pour:
// Récupérer les valeurs de chaque slider
// Appeler la fonction addMove avec les composants de la couleur du fond, puis les trois valeurs récupérées
// Cette fonction est appelée à chaque appui sur le bouton proposition
function submitInput () {
    ...
    
    addMove(...)
}

// Ajout du coup joué avec son score et son statut.
function addMove (r, g, b, rr, gg, bb) {
    // On récupère la proposition
    let rgbHex = hex(rr, gg, bb)

    // Est-ce que on gagne ?
    // On teste les égalités entre rr et r, gg et g, bb et b
    // && veut dire **et** en JS
    // === veut dire **égal** en JS
    let won = rr === r && ... && ... 

    //Mise à jour du fond de l'élément résultat
    result.style ... = ...
    // Mise à jour du contenu de l'élément résultat
    result.innerHTML = 'Couleur:' + ...

    // On ajuste le message selon si on gagne ou non
    if (won) {
        div.innerHTML += 'Bravo !' //M
        //On change le style pour montrer que c'est gagné.
        div.style.border = 'thin solid #' +  '333'
        win()
    } 
    else { //M
	    let scored = score(r, g, b, rr, gg, bb) //M
        div.innerHTML += ' (' + scored + '% identique)' //M
    }
}

// Si on a gagné,on change les boutons de activé vers désactivé.
//Donnée
function win () {
    
    //Activer le bouton "recommencer"
    //restart.disabled = ...
    //Désactiver le bouton soumettre (Proposition ...)
    submit.disabled = true
}

// Calcul de l'écart euclidien entre rgb et rrggbb
function score (r, g, b, rr, gg, bb) {
    let maxRErr = Math.max(r, 15 - r)
    let maxGErr = Math.max(g, 15 - g)
    let maxBErr = Math.max(b, 15 - b)
    let maxDist = Math.sqrt(maxRErr * maxRErr +
                              maxGErr * maxGErr +
                              maxBErr * maxBErr)
    let rErr = Math.abs(rr - r)
    let gErr = Math.abs(gg - g)
    let bErr = Math.abs(bb - b)
    let dist = Math.sqrt(rErr * rErr +
			   gErr * gErr +
			   bErr * bErr)
    return Math.floor(100 * (1 - dist / maxDist))
}

//Écriture de la couleur (r,g,b) en hexadécimal 3 lettres.
//Donnée
function hex (r, g, b) {
    let rs = r.toString(16)
    let gs = g.toString(16)
    let bs = b.toString(16)
    return ('#' + rs + gs + bs).toUpperCase()
}

// Nombre aléatoire entier entre 0 et max exclu.
function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

```

### Extensions

Pour aller plus loin, vous pouvez implémenter les comportements suivants:

* affichage du nombre d'essais;
* affichage la liste des valeurs essayées;
* afficher un bouton recommencer après la fin du jeu;
* faire un bravo plus sympathique (animation css) (non fourni);
* partager son score sur les réseaux sociaux (non fourni).

### Correction du jeu

```html title="index.html"
<!--
    Guess My RGB 0.1.0
    Source: https://github.com/susam/myrgb
    Copyright (c) 2024 Susam Pal

    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    Modifié par Marius Monnier dans le cadre de l'enseignement de NSI.
    04/04/2024
    
  -->
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Devine mon RGB</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Un jeu de devinettes.">
    <link rel="stylesheet" href="style.css">
    <script src="interaction.js"></script>
  </head>
  <body onload="init()">
    <main>
      <h1>Devine la couleur</h1>
      <ol class="ruler">
        <li>0</li>
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
        <li>8</li>
        <li>9</li>
        <li>A</li>
        <li>B</li>
        <li>C</li>
        <li>D</li>
        <li>E</li>
        <li>F</li>
    </ol>
    <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="rin"
            onchange="ecrireCouleur()" oninput="ecrireCouleur()">
        <label for="rin">R</label>
      </div>
    <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="gin"
            onchange="ecrireCouleur()" oninput="ecrireCouleur()">
        <label for="gin">R</label>
      </div>
      <div class="slider">
        <input type="range" min="0" max="15" step="1" value="2" id="bin"
            onchange="ecrireCouleur()" oninput="ecrireCouleur()">
        <label for="bin">R</label>
      </div>
      <p id="info">Positionnez les sliders pour essayer de trouver la couleur de fond.</p>
      <button id="submit"  onclick="afficherCouleur()">Proposition : </button>
    </main>
    <div id="result"></div>
  </body>
</html>

```

```css title="style.css"
/* *** Partie fixe pour un affichage "satisfaisant" *** */
body {
    width: 100%;
    padding: 0;
    margin: 0;
}
ol.ruler {
    padding: 0;
    display: flex;
    width: calc(100% - 1em);
    justify-content: space-between;
}
div.slider {
    display: flex;
    width: 100%;
}
ol.ruler li {
    list-style-type: none;
}
div.slider input {
    width: 100%;
}

/* *** Partie Modifiable en partie *** */

main {
    background-color: white; /* Un fond uni permettrait de mieux voir les sliders */
    padding: 1em;
    margin: 0;
}
h1 {
    /* à vous de styliser le titre de la page. */
    text-decoration: underline;
}

button {
    /* Stylisez les boutons de la page comme vous le voulez */
}

#rin {
    accent-color: #f00; /*Couleur de la barre du slider rouge */
}

#gin {
    accent-color: #0f0; /*Couleur de la barre du slider vert */
}

#bin {
    accent-color: #00f; /*Couleur de la barre du slider bleu */
}
```


```javascript title="interaction.js"
'use strict'

//La couleur du fond décomposée en RGB.
let r,g,b;

// Nos trois sliders
let rin, gin, bin;

//Les éléments html que nous mettons à jour.
let info, submit, restart, result;

// Nombre d'essais.
//let ... = ... ;

function init () {
    /* Fonction appelée au chargement de la page pour créer toutes les variables HTML */
    
    //Liaison des variables JS avec les éléments HTML
    rin = document.getElementById('rin')
    gin = document.getElementById('gin')
    bin = document.getElementById('bin')
    
    //Donnée
    info = document.getElementById('info')
    submit = document.getElementById('submit')
    restart = document.getElementById('restart')
    result = document.getElementById('result')

    //Appel de la fonction qui met à jour le fond de la page et l'état des boutons
    setupGame()
}

function pickRGB () {
    //Choix d'une quantité aléatoire pour chaque composante entre 0 et F
    
    r = getRandomInt(15)
    g = getRandomInt(15)
    b = getRandomInt(15)
}

//Initiatialisation des variables et de la couleur.
function setupGame () {

    //Appel de la fonction qui choisit une couleur aléatoire dans r,g et b
    pickRGB()
    
    // Désactivation du bouton envoyer et redémarrer au début du jeu.
    submit.disabled = true
    restart.disabled = true

    // Aucun résultat et aucun essai.
    result.innerHTML = ''
    

    //La couleur de fond est obtenue grâce à hex, appelée avec r, g et b
    document.body.style.background = hex(r,g,b)
}

// Lecture des sliders et mise à jour des variables globales.
// On lit chaque slider dans une variable.
// Puis on met à jour l'élément submit
function ecrireCouleur () {
    let vr = rin.valueAsNumber;
    let vg = gin.valueAsNumber;
    let vb = gin.valueAsNumber;
    submit.innerHTML = "Proposition : "+hex(vr,vg,vb);
    //On peut désormais cliquer dessus.
    submit.disabled = false;
}

// Fonction utilitaire pour:
// Récupérer les valeurs de chaque slider
// Appeler la fonction addMove avec les composants de la couleur du fond, puis les trois valeurs récupérées
// Cette fonction est appelée à chaque appui sur le bouton proposition
function afficherCouleur () {
    let vr = rin.valueAsNumber;
    let vg = gin.valueAsNumber;
    let vb = gin.valueAsNumber;
    
    addMove(vr,vg,vb,rr,gg,bb);
}

// Ajout du coup joué avec son score et son statut.
function addMove (r, g, b, rr, gg, bb) {
    // On récupère la proposition
    let rgbHex = hex(rr, gg, bb)

    // Est-ce que on gagne ?
    // On teste les égalités entre rr et r, gg et g, bb et b
    // && veut dire **et** en JS
    // === veut dire **égal** en JS
    let won = rr === r && gg === g && bb === b; 

    //Mise à jour du fond de l'élément résultat
    result.style.backgroundColor = rgbHex
    // Mise à jour du contenu de l'élément résultat
    result.innerHTML = 'Couleur:' + rgbHex

    // On ajuste le message selon si on gagne ou non
    if (won) {
        div.innerHTML += 'Bravo !' //M
        //On change le style pour montrer que c'est gagné.
        div.style.border = 'thin solid #' +  '333'
        win()
    } 
    else { //M
	    let scored = score(r, g, b, rr, gg, bb) //M
        div.innerHTML += ' (' + scored + '% identique)' //M
    }
}

// Si on a gagné,on change les boutons de activé vers désactivé.
//Donnée
function win () {
    
    //Activer le bouton "recommencer"
    //restart.disabled = ...
    //Désactiver le bouton soumettre (Proposition ...)
    submit.disabled = true
}

// Calcul de l'écart euclidien entre rgb et rrggbb
function score (r, g, b, rr, gg, bb) {
    let maxRErr = Math.max(r, 15 - r)
    let maxGErr = Math.max(g, 15 - g)
    let maxBErr = Math.max(b, 15 - b)
    let maxDist = Math.sqrt(maxRErr * maxRErr +
                              maxGErr * maxGErr +
                              maxBErr * maxBErr)
    let rErr = Math.abs(rr - r)
    let gErr = Math.abs(gg - g)
    let bErr = Math.abs(bb - b)
    let dist = Math.sqrt(rErr * rErr +
			   gErr * gErr +
			   bErr * bErr)
    return Math.floor(100 * (1 - dist / maxDist))
}

//Écriture de la couleur (r,g,b) en hexadécimal 3 lettres.
//Donnée
function hex (r, g, b) {
    let rs = r.toString(16)
    let gs = g.toString(16)
    let bs = b.toString(16)
    return ('#' + rs + gs + bs).toUpperCase()
}

// Nombre aléatoire entier entre 0 et max exclu.
function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}
```