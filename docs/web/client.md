Les deux présentations faites en cours:

* <https://codimd.apps.education.fr/p/arePnTswn#/>
* <https://codimd.apps.education.fr/p/o2n9iWXGI#/>

Les deux tp prévus, seul le premier a été abordé:

* [Labodemaths.fr](https://labodemaths.fr/WordPress3/nsi-js-et-html-tp2/)
* [Qzkz.xyz](https://qkzk.xyz/docs/nsi/cours_premiere/ihm_web/js/)

# Projet Web - Femme scientifique

Vous devez produire un site web traitant d'une femme, dans un domaine scientifique.
Pour cela vous devez programmer un **site statique** valide (au sens du validateur **w3c**).

## Partie 1 - Le fond

Votre projet doit ressembler au fichier suivant (donnée sur pronote)

[La page PDF imprimée](./production.pdf)

Vous devez utiliser les **balises sémantiques vues en cours**.

Pensez à ajouter:

* Une section mots clés
* Une section Domaine de recherche (les deux dans la fiche d'identité)
* Une image **libre de droit** avec **un texte alternatif**.
* Des liens vers vos sources et toute ressource que vous jugez utile

## Partie 2 - La forme

**Il est possible que vous deviez modifier le HTML pour obtenir le bon rendu**.

Vous êtes libres sur les couleurs mais votre site doit être organisé de la façon suivante:

* Le titre en haut et centre de page.
* Sur la première ligne, la biographie
* Sur la deuxième ligne, la fiche d'identité et des images, **séparés par de légers espaces**.
* Sur la troisième ligne, une frise chronologique **horizontale** qui prend tout l'espace.
* Sur les lignes du dessous, entre 7 et 10 questions sur la scientifique.

Vous allez avoir besoin du modèle **flexbox**, voici quelques liens pour l'expliquer:

* <https://developer.mozilla.org/fr/docs/Learn/CSS/CSS_layout/Flexbox>
* <https://css-tricks.com/snippets/css/a-guide-to-flexbox/>
* <https://fr.w3docs.com/apprendre-css/the-ultimate-guide-to-flexbox.html>
* <https://la-cascade.io/articles/flexbox-guide-complet>

N'hésitez pas à chercher des idées sur le web pour faire vos styles CSS.
Cependant, si vous **copiez** un morceau de code, vous devez préciser sa source en commentaire.

## Partie 3 - Prolongements

Pour rendre votre site plus **agréable**:

* Utilisez du JS pour obtenir un rendu *flashcard* sur vos questions (on retourne la carte au clic)
* Faites une charte graphique cohérente en utilisant des variables CSS et une palette de couleur.
* Ajoutez un mode sombre et un bouton **toggle**
* Ajoutez un **changeur de taille de police**, en faisant attention à garder un site agréable.

### Quelques liens pour vous aider:

* Faire des flashcards en CSS <https://www.w3schools.com/howto/howto_css_flip_card.asp> ou <https://www.vincent-barrault.fr/articles/effet-flip-card-avec-css>
* Variables CSS: <https://developer.mozilla.org/fr/docs/Web/CSS/Using_CSS_custom_properties> et Palette de couleurs: <https://www.palettedecouleur.net/>
* Mode sombre: <https://stacklima.com/comment-creer-un-mode-sombre-pour-les-sites-web-en-utilisant-html-css-javascript/>

## Notation

### La forme (8 points)

| Critère                            | Non fait (0%)      | Presque (50%)                         | Parfait (100%)                                  | Note maximale |
| :--                                | :--                | :--                                   | :--                                             | :--           |
| La page contient des liens         | Aucun lien externe | Liens sans rapport                    | Plusieurs liens, en rapport avec le sujet       | 2             |
| La page respecte le format attendu | Aucun CSS          | On distingue les lignes demandés      | La page est entièrement stylisée comme demandée | 3             |
| La page est colorée                | Noir et blanc      | Titres colorés                        | Fond des cartes questions et frises colorés     | 1             |
| Le lignes sont **flexibles**       | Pas de lignes      | Pas de marges ou Utilisation de float | Utilisation de flexbox et marges correctes      | 2             |

### Le fond (7 points)

| Critère | Pas fait(0%) | Presque(50%) | Parfait(100%) | Note max |
| --      | --           | --           | --            | --       |
| La biographie est **originale**               | Copiée de Wikipédia            | Extraits de Wikipédia                     | Originale (peut être inspirée des sources de wiki)  |    1      |
| Questions pertinentes                         | Questions sans rapport         | Questions en rapport mais trop simples    | Questions de difficultés variées                    |    2      |
| Frise chronologique                           | Non présente ou une seule date | Succincte avec évènements peu spécifiques | Construite en réfléchissant aux moments importantes |    2      |
| La fiche d'identité est complète            | Non présente ou non remplie | Quelques sections remplies       | Entièrement remplie                             |             2  |

### Critères techniques (10 points)

| Critère                                                              | 0%                                           | 50%                                   | 100%                                                           | Maximum |
| --                                                                   | --                                           | --                                    | --                                                             | --      |
| Le rendu contient bien des fichiers séparés **dans une archive ZIP** | Un seul fichier html                         |                                       | Un fichier HTML, un fichier CSS (JS si fait)                   | 1       |
| Le code est valide sur <https://validator.w3.org>                    | Code non valide                              | Erreurs à corriger                    | Moins de 2 erreurs de validations (Warning=erreur)             | 1       |
| Expression écrite                                                    | Beaucoup de fautes et textes non lisibles    | Quelques fautes de syntaxe            | Moins de 3 fautes                                              | 2       |
| Encodage                                                             | Le fichier ne déclare pas d'encodage         | Encodage Latin-1                      | Encodage UTF-8                                                 | 1       |
| L'image est accompagnée d'un texte alternatif                        | Pas de texte alternatif                      | Texte présent mais peu utile          | Texte utile et lisible par un lecteur d'écran                  | 2       |
| Le code CSS est **commenté**.                                        | Aucun commentaire et code peu compréhensible | Code compréhensible mais pas commenté | Code commenté et bien séparé (classes, id, ordre des éléments) | 2       |
| Le code HTML est **sémantique**.                                     | Utilisation massive de div et span           | Quelques div                          | Aucun élément div et éléments section, aside, ...              | 1        |

Une pénalité de 1 points par jour de retard sera appliquée.
