 Nous allons ici détailler les éléments historiques et scientifiques ayant mené à la construction des ordinateurs tels que nous les connaissons.
 
!!! note "Inspirations et autres sources"
	Ce support de cours est librement inspiré des suivants:
	
	* <https://lycee.educinfo.org/index.php?activite=architecture&page=introduction>
	* [Le jeu des 7 familles de l'informatique](https://interstices.info/famille-machines-composants/)
	* Les cours afférents proposés à l'accueil.
	* [L'ordinateur par Interstices](https://interstices.info/lordinateur-objet-du-siecle/)
	* [Ressources transversales historiques](https://interstices.info/le-patrimoine-numerique-entre-enjeux-materiels-et-immateriels/)
	* Le livre "Du boulier à la révolution numérique", aux éditions RBA par Vincenç Torra.
	* Un billet sur les temps d'accès à la mémoire <https://zestedesavoir.com/billets/2909/le-microprocesseur-ce-monstre-de-puissance-qui-passe-son-temps-a-attendre/>

Le TP et ses documents:

* [TP](./tp-archi.pdf)
* [Von neumann incomplet](./von-neuman-incomplet.svg)
* [Carte mère incomplète](./carte-mere-incomplete.svg)


# Cours

Le cours de <https://clogique.fr> est complet et facile à lire, il est disponible à l'adresse suivante: <https://clogique.fr/nsi/premiere/VonNeumann/>.

# À retenir:

!!! warning "Avant de lire ceci"
	Vous devez avoir lu et compris le cours sur l'architecture de Von Neumann.
	Ce qui suit n'est qu'un résumé partiel et **insuffisant**.

## Historique

Les machines à calculer manuelles ont été introduites dès l'antiquité.
Elles ont d'abord servies comme aide pour décharger la mémoire lors des calculs complexes.
À partir du XVII^ème^ siècle, on met au points les premiers instruments mécanique de calcul, comme les règles à calcul ou la Pascaline.
Au XIX^ème^ siècle, Charles Babbage tente la mise au point d'une machine à calculer entièrement mécanique, malheureusement trop coûteuse.

À partir du XX^ème^ siècle, les premiers *calculateurs* naissent avec les guerres mondiales.
Alan Turing, mathématicien Anglais met au point le modèle théorique de l'ordinateur que nous utilisons encore aujourd'hui.
John Von Neumann, mathématicien Americano-hongrois mit au point le modèle *pratique* de l'ordinateur dans le cadre du Projet Manhattan.

## Le modèle de Von Neumann

Le modèle de Von Neumann décrit un ordinateur comme 4 composants principaux.

![Les liens entre les composants](http://monlyceenumerique.fr/nsi_premiere/archios_arse/dl/von_neumann_architecture.png)

* L'unité de contrôle permet d'exécuter un programme, avec ses boucles et ses conditions.
* L'unité arithmétique permet d'effectuer les calculs sur les données.
* La mémoire permet de stocker les données et les programmes à exécuter.
* Les entrées / sorties, pour communiquer avec la machine.

Aujourd'hui, même s'ils sont plus évolués, les ordinateurs respectent toujours cette architecture.

### Zoom sur les composants : La mémoire

La mémoire d'un ordinateur peut être vue comme un *ensemble de cellules* qui stockent des données.
Chaque cellule est capable de stocker **un unique octet**.

??? example "Combien de valeurs différentes peut prendre une cellule mémoire ?"
	Elle stocke un octet, donc 8 bits, donc $`2^8-1=255`$ valeurs différentes.

On attribue à chaque cellule une **adresse mémoire**, en commençant à 0.

??? example "Si on a 256 cellules, quelle est l'adresse de la dernière cellule ?"
	Vu que l'on compte depuis 0, la dernière cellule est situé à l'adresse $`256-1=255`$.

Une cellule peut être accédée en **lecture** ou **écriture**:

* Si on lit la cellule à l'adresse 24, on obtient son contenu.
* Si on écrit $`72`$ à l'adresse 24, la cellule contient désormais le nombre $`72`$.

Les deux mémoires principales sont:

* La RAM ou *mémoire vive* qui est **volatile**, elle est effacée quand on éteint l'ordinateur
* La mémoire morte qui est **persistante**, elle survit à l'arrêt de l'ordinateur.

### Zoom sur les composants : L'Unité Arithmétique et Logique

L'UAL est un ensemble de circuits logiques et de petites mémoires appelées **registres**.

* Les circuits permettent de faire des opérations **arithmétiques, logiques, des comparaisons et des opérations sur les bits**.
* Les registres permettent de stocker des données et des résultats.

### Zoom sur les composants: L'Unité de Contrôle

L'UC est elle aussi composée de circuits et de registres.
Elle se charge de lire le programme en mémoire et de **décoder et exécuter** les instructions.

Elle est **cadencée** par une horloge présente dans le processeur, qui la fait s'exécuter à une fréquence donnée, en Hz.

## Assembleur

Un ordinateur ne parle ni français ni python, uniquement **binaire**.
Pour exécuter un programme, il faudrait donc **l'écrire en binaire** et le mettre en mémoire.

Au lieu de faire ça, nous utilisons un langage plus simple (le python par exemple), qui est ensuite **traduit en binaire** pour être exécuté.
Dans certains cas on doit cependant écrire en **binaire** pour utiliser au mieux les capacités du processeur.

Au lieu d'écrire directement en binaire, on utilise un langage dit **d'assemblage** ou **assembleur**.
Ce langage est **spécifique à chaque processeur** et est **extrêmement limité**.

Les opérations en assembleur sont écrites sous la forme `opération opérande1 opérande2 ...`.
Elles manipulent directement: l'UAL, la mémoire, les registres.
En assembleur, nous n'avons pas de **variables** ou de **types** il faut **tout refaire nous même**.

<!-- Cours à développer plus tard

# Architecture d'un ordinateur

## Avant l'ordinateur

### Prémices historiques

!!! info "La vidéo qui va bien"
	<https://videos.univ-grenoble-alpes.fr/video/16317-histoire-de-linformatique-les-calculateurs-mecaniques-de-pascal-a-leniac/>

Dès l'antiquité, le besoin de *calculer* était présent.
Parmi les premières traces de ce besoin, les **bouliers** était utilisés par la plupart des civilisations de l'antiquité.

Babbage et Lovelace
Pascal et la Pascaline


### Prémices théoriques

Alan Turing, Enigma.

!!! info "Sur Alan Turing"
	Un article très intéressant sur Interstices expliquant ses contributions ainsi que les problèmes que son époque lui apporta.
	[L'article](https://interstices.info/turing-les-images-dune-imitation/)

Alonzo Church, Lambda Calcul

## Premiers balbutiements
### Von neumann et l'Eniac

[Interstices](https://interstices.info/le-modele-darchitecture-de-von-neumann/)

!!! tip "Les gagnants écrivent l'histoire"
	Konrad suze et le Z3, voir 
### Architecture Harvard

![Une image de l'architecture Harvard](no-pic.png)

Pourquoi on ne l'a pas gardé

### Architecture Von Neumann

![Une image de l'architecture Von Neumann](no-pic.png)

Pourquoi on l'a gardé

#### La mémoire

#### Le calcul

#### Les communications

## Ordinateurs d'aujourd'hui
### La danse des coeurs
Super calculateurs et machines multiprocesseurs
### Les systèmes embarqués
Besoins réduits, contraintes fortes
## Ordinateurs de demain
### Quantique ou non


# À retenir

Les machines à calculer manuelles ont été introduites dès l'antiquité.
Elles ont d'abord servies comme aide pour décharger la mémoire lors des calculs complexes.
À partir du XVII^ème^ siècle, on met au points les premiers instruments mécanique de calcul, comme les règles à calcul ou la Pascaline.
Au XIX^ème^ siècle, Charles Babbage tente la mise au point d'une machine à calculer entièrement mécanique, malheureusement trop coûteuse.

À partir du XX^ème^ siècle, les premiers *calculateurs* naissent avec les guerres mondiales.
Alan Turing, mathématicien Anglais met au point le modèle théorique de l'ordinateur que nous utilisons encore aujourd'hui.
John Von Neumann, mathématicien Americano-hongrois mit au point le modèle *pratique* de l'ordinateur dans le cadre du Projet Manhattan.

Le modèle de Von Neumann décrit un ordinateur comme 4 composants principaux.

![Les liens entre les composants](http://monlyceenumerique.fr/nsi_premiere/archios_arse/dl/von_neumann_architecture.png)

* L'unité de contrôle permet d'exécuter un programme, avec ses boucles et ses conditions.
* L'unité arithmétique permet d'effectuer les calculs sur les données.
* La mémoire permet de stocker les données et les programmes à exécuter.
* Les entrées / sorties, pour communiquer avec la machine.

Aujourd'hui, même s'ils sont plus évolués, les ordinateurs respectent toujours cette architecture.

-->
