### Un Morpion en python
# Auteur: Marius Monnier
# Licence CC BY-NC-SA 4.0 :
#	- Attribution
#	- Pas d’Utilisation Commerciale
#	- Partage dans les Mêmes Conditions

## On définit toutes les variables *globales*

VIDE = ' ' # Pour représenter une case vide
GRILLE_DU_MORPION = [[VIDE,VIDE,VIDE] for i in range(3)] # La grille du morpion

SIGNE_J1 = 'X'
SIGNE_J2 = 'O'

# Les délimiteurs pour les lignes et les colonnes
DELIM_LI = '-'
DELIM_COL= '|'

## On définit les fonctions utiles pour notre jeu, en utilisant les variables ci dessus.

def joue(joueur,ligne,colonne):
    """
    Met la marque d'un joueur sur la case
    aux coordonnées (ligne,colonne)

    Le numéro du joueur est 1 ou 2.
    La ligne et la colonne doivent être entre 0 et 2 inclus

    Renvoie True si la marque a été mise, False sinon.
    """


    if GRILLE_DU_MORPION[ligne][colonne] != VIDE: # Cette case est incorrecte
        print("Quelqu'un a déjà joué ici !")
        return False

    if joueur != 1 and joueur != 2: # Le joueur est incorrect
        print("Joueur inexistant !")
        return False


    if joueur == 1: # On met le signe 1 pour le joueur 1
        GRILLE_DU_MORPION[ligne][colonne] = SIGNE_J1
    else: #Ici joueur est 2 obligatoirement
        GRILLE_DU_MORPION[ligne][colonne] = SIGNE_J2

    return True # On a réussi à mettre le signe, donc on renvoit True


def est_une_ligne_gagnante(ligne,signe_joueur):
    """
    Teste si le joueur a gagné sur la ligne numéro ligne
    signe_joueur doit être SIGNE_J1 ou SIGNE_J2.
    ligne doit être compris entre 0 et 2 inclus.
    Renvoie True si il a gagné ici, False sinon
    """
    return GRILLE_DU_MORPION[ligne][0] == signe_joueur and \
            GRILLE_DU_MORPION[ligne][1] == signe_joueur and \
            GRILLE_DU_MORPION[ligne][2] == signe_joueur

def est_une_colonne_gagnante(colonne,signe_joueur):
    """
    Teste si le joueur a gagné sur la colonne numéro colonne
    colonne doit être compris entre 0 et 2 inclus.
    Le signe doit être SIGNE_J1 ou SIGNE_J2.
    Renvoie True si il a gagné ici, False sinon
    """
    return GRILLE_DU_MORPION[0][colonne] == signe_joueur and \
            GRILLE_DU_MORPION[1][colonne] == signe_joueur and \
            GRILLE_DU_MORPION[2][colonne] == signe_joueur

def est_diagonale_gagnante(signe_joueur):
    """
    Teste si le joueur a gagné avec une diagonale.
    signe_joueur doit être SIGNE_J1 ou SIGNE_J2.

    Renvoie True s'il a gagné sinon False.
    """
    # La première diagonale est celle qui part d'en haut et descend
    diagonale_gauche_droite_gagne = GRILLE_DU_MORPION[0][0] == signe_joueur and \
        GRILLE_DU_MORPION[1][1] == signe_joueur and \
        GRILLE_DU_MORPION[2][2] == signe_joueur

    diagonale_droite_gauche_gagne = GRILLE_DU_MORPION[0][2] == signe_joueur and \
        GRILLE_DU_MORPION[1][1] == signe_joueur and \
        GRILLE_DU_MORPION[2][0] == signe_joueur

    # On gagne si l'une des deux est remplie.
    return diagonale_gauche_droite_gagne or diagonale_droite_gauche_gagne

def a_gagne(joueur):
    """
    Renvoie vrai si le joueur a gagné.
    joueur doit être 1 ou 2.
    """

    # Selon le joueur qu'on veut tester, on prend son bon signe.
    signe = ''
    if joueur == 1:
        signe = SIGNE_J1
    elif joueur == 2:
        signe = SIGNE_J2
    else:
        print("Joueur impossible")
        return False
    # On gagne si une diagonale, une ligne ou une colonne est remplie.
    return est_diagonale_gagnante(signe) or \
        est_une_ligne_gagnante(0,signe) or est_une_ligne_gagnante(1,signe) or est_une_ligne_gagnante(2,signe) or \
        est_une_colonne_gagnante(0,signe) or est_une_colonne_gagnante(1,signe) or est_une_colonne_gagnante(2,signe)

def affiche_plateau():
    """ Affiche un plateau de morpion "joliment" """

    # On affiche chaque ligne
    for ligne in range(3):
        # Séparer chaque ligne par : -------
        print('-'*7)
        # Afficher chaque colonne d'une ligne : |X|X|X| par exemple
        for colonne in range(3):
            print(f'|{GRILLE_DU_MORPION[ligne][colonne]}', end='')
        print('|') # Afficher la dernière barre et passer à la ligne suivante
    print('-'*7) # Fermer le tableau

## Début de la boucle principale du programme

# On initialise les variables qui représente qui a gagné et qui joue
joueur_gagnant = 0
joueur_courant = 1

affiche_plateau()

# Tant que personne n'a gagné
while joueur_gagnant == 0:
    print("\t\tÀ vous de jouer joueur ",joueur_courant)
    ligne = int(input("Dans quelle ligne ?"))
    colonne = int(input("Dans quelle colonne ?"))

    # Le joueur pose une marque
    joue(joueur_courant,ligne,colonne)

    # Si il a gagné on met à jour le gagnant, sinon on change de joueur courant
    if a_gagne(joueur_courant):
        joueur_gagnant = joueur_courant
    elif joueur_courant == 1:
        joueur_courant = 2
    else:
        joueur_courant = 1
    affiche_plateau()

# Quand on arrive ici, joueur_gagnant est 1 ou 2, donc quelqu'un a gagné.

print("\t\tBravo joueur ", joueur_gagnant)
    
